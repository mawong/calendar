package calendar;

/**
 * Properties that are loaded from properties.xml. Provides custom labels and other
 * UI information for the calendar application.
 * 
 * @author MAVIRI
 */
public enum CAL_PropertyType {
    //title
    PROP_APP_TITLE,
    
    //labels
    SUNDAY_LABEL,
    MONDAY_LABEL,
    TUESDAY_LABEL,
    WEDNESDAY_LABEL,
    THURSDAY_LABEL,
    FRIDAY_LABEL,
    SATURDAY_LABEL,
    TIME_LABEL,
    MONTH_EVENTS_LABEL,
    DAY_EVENTS_LABEL,
    CHANGE_MONTH_TITLE,
    CHANGE_MONTH_LABEL,
    CHANGE_YEAR_TITLE,
    CHANGE_YEAR_LABEL,
    EVENTS_DIALOG_TITLE,
    EVENTS_DIALOG_LABEL,
    ADD_EVENT_TITLE,
    EDIT_EVENT_TITLE,
    REMOVE_EVENT_TITLE,
    
    //button text
    NEW_TEXT,
    LOAD_TEXT,
    SAVE_TEXT,
    EXIT_TEXT,
    PREV_TEXT,
    NEXT_TEXT,
    CHANGE_MONTH_TEXT,
    CHANGE_YEAR_TEXT,
    ADD_EVENT_TEXT,
    EDIT_EVENT_TEXT,
    REMOVE_EVENT_TEXT,
    
    //tooltips for buttons
    NEW_TOOLTIP,
    LOAD_TOOLTIP,
    SAVE_TOOLTIP,
    EXIT_TOOLTIP,
    PREV_MONTH_TOOLTIP,
    NEXT_MONTH_TOOLTIP,
    CHANGE_MONTH_TOOLTIP,
    CHANGE_YEAR_TOOLTIP,
    DATE_BUTTON_TOOLTIP,
    ADD_EVENT_TOOLTIP,
    EDIT_EVENT_TOOLTIP,
    REMOVE_EVENT_TOOLTIP,
    
    //messages (dialogs)
    NO_MONTH_CHOICE_MESSAGE,
    INVALID_YEAR_CHOICE_MESSAGE,
    NO_YEAR_OPTION_CHOICE_MESSAGE,
    REMOVE_EVENT_MESSAGE,
    INVALID_TIME_DIGIT_MESSAGE,
    INVALID_TIME_NUMBERS_MESSAGE,
    NO_PERIOD_PICKED_MESSAGE,
    TEXT_FIELD_EMPTY_MESSAGE,
    END_TIME_BEFORE_START_TIME_MESSAGE,
    CANNOT_EDIT_EVENT_MESSAGE,
    CANNOT_REMOVE_EVENT_MESSAGE,
    INVALID_EDIT_REMOVE_TEXT_FIELD_MESSAGE,
    CALENDAR_SAVE_UNSAVED_WORK_MESSAGE,
    CALENDAR_SAVED_MESSAGE,
    CALENDAR_NAME_EMPTY_MESSAGE,
    
    //errors
    NEW_CALENDAR_ERROR_MESSAGE,
    CALENDAR_SAVED_ERROR_MESSAGE,
    CALENDAR_LOAD_ERROR_MESSAGE,
    CALENDAR_EXIT_ERROR_MESSAGE,
    CALENDAR_PROPERTIES_FILE_ERROR_MESSAGE
}