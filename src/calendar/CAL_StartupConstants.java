package calendar;

/**
 * Stores all the constants used by the calendar application.
 *
 * @author MAVIRI
 */
public class CAL_StartupConstants {
    //constants for the data and properties
    public static final String PROPERTIES_FILE_NAME = "properties.xml";
    public static final String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";    
    public static final String PATH_DATA = "./data/";
    public static final String PATH_CALENDAR = PATH_DATA + "cal/";
    public static final String PATH_CSS = "calendar/css/";
    
    //error constant
    public static final String OK_BUTTON_LABEL = "OK";
}