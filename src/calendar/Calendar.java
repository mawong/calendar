package calendar;

import static calendar.CAL_PropertyType.*;
import static calendar.CAL_StartupConstants.*;
import calendar.error.ErrorHandler;
import calendar.file.JsonCalendarFileManager;
import calendar.gui.CAL_GUI;
import java.util.Locale;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;

/**
 * This is a JavaFX application that allows a user to view a calendar and to add
 * any necessary items (s)he desires in any day or month.
 * 
 * @author MAVIRI
 */
public class Calendar extends Application {
    //full user interface
    CAL_GUI gui;
    
    /**
     * This method starts the application after the main method calls launch(args).
     */
    @Override
    public void start(Stage primaryStage) {
        //create the error handler
        ErrorHandler eh = ErrorHandler.getErrorHandler();
        eh.initMessageDialog(primaryStage);
        
        //loads the properties
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
            JsonCalendarFileManager jsonFileManager = new JsonCalendarFileManager();
            gui = new CAL_GUI(primaryStage);
            gui.setCalendarFileManager(jsonFileManager);
            gui.initGUI(appTitle);
        }
        else
            eh.handlePropertiesFileError();
    }
    
    /**
     * Loads the properties file
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            //load the settings to start the app
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            return false;
        }        
    }
    
    /**
     * Starts the application with this method. The launch(args) line starts the app
     * by going to the start method above.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }
}