package calendar.controller;

import calendar.gui.CAL_GUI;
import calendar.gui.ChangeMonthDialog;
import calendar.gui.ChangeYearDialog;
import calendar.gui.DayEventsDialog;
import calendar.gui.MessageDialog;
import calendar.gui.YesNoCancelDialog;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * This class helps us with anything that has to do with the calendar: change the current month,
 * change the current year, and even adding, editing, or removing events from the calendar.
 *
 * @author MAVIRI
 */
public class CalendarController {
    MessageDialog md;
    ChangeMonthDialog cmd;
    ChangeYearDialog cyd;
    DayEventsDialog ded;
    String monthToChange;
    int yearToChange;
    
    /**
     * Constructor that creates the calendar controller.
     * 
     * @param primaryStage - the stage the user is viewing
     * @param initMD - the message dialog
     * @param initYNCD - the yes/no/cancel dialog
     * @param initHC - the hash controller
     * @param initGUI - the GUI 
     */
    public CalendarController(Stage primaryStage, MessageDialog initMD, YesNoCancelDialog initYNCD, EventController initHC, CAL_GUI initGUI) {
        cmd = new ChangeMonthDialog(primaryStage, initMD);
        cyd = new ChangeYearDialog(primaryStage, initMD);
        ded = new DayEventsDialog(primaryStage, initMD,initGUI, initHC, initYNCD);
        monthToChange = "";
        md = initMD;
    }
    
    /**
     * This method handles getting the next or previous month of the year.
     * 
     * @param gui - the GUI
     * @param prevOrNext - handling previous (0) or next (1)
     */
    public void handlePrevOrNextButton(CAL_GUI gui, int prevOrNext) {
        //regardless, we want to remove the current day button
        gui.removeCurrentDayButton();
        
        //get what we need
        int intEditedMonth = gui.getIntEditedMonth();
        int editedYear = gui.getEditedYear();
        String editedMonth = gui.getEditedMonth();
        
        //previous month
        if (prevOrNext == 0) {
            if (intEditedMonth == 1) {
                intEditedMonth = 12;
                editedYear--;
            }
            else
                intEditedMonth--;
            editedMonth = gui.getMonth(intEditedMonth, editedYear);
        }
        //next month
        else if (prevOrNext == 1) {
            if (intEditedMonth == 12) {
                intEditedMonth = 1;
                editedYear++;
            }
            else
                intEditedMonth++;
            editedMonth = gui.getMonth(intEditedMonth, editedYear);
        }
        gui.setIntEditedMonth(intEditedMonth);
        gui.setEditedYear(editedYear);
        gui.setEditedMonth(editedMonth);
        gui.updateMonthLabel();
        gui.disableAllButtons();
        gui.calendarTableEvents();
    }
    
    /**
     * This method handles the change month option. We open a dialog box, and the 
     * user changes to the month he or she wishes to go to.
     * 
     * @param gui - the GUI
     */
    public void handleChangeMonthRequest(CAL_GUI gui) {
        cmd.showChangeMonthDialog();
        
        //did the user confirm?
        if (cmd.wasCompleteSelected()) {
            //change the month
            monthToChange = cmd.getMonth();
            gui.setEditedMonth(monthToChange);
            gui.setIntEditedMonth(gui.getMonth(monthToChange));
            gui.removeCurrentDayButton();
            gui.updateMonthLabel();
            gui.disableAllButtons();
            gui.calendarTableEvents();
        }
        else {
            //do nothing because the user hit cancel
        }
    }
    
    /**
     * This method handles the change year option. We open a dialog box, and the 
     * user changes to the year he or she wishes to go to.
     * 
     * @param gui - the GUI
     */
    public void handleChangeYearRequest(CAL_GUI gui) {
        cyd.showChangeYearDialog();
        
        //did the user confirm?
        if (cyd.wasCompleteSelected()) {
            //change the year
            yearToChange = cyd.getYear();
            gui.setEditedYear(yearToChange);
            if (gui.getIntEditedMonth() == 2) {
                if ((yearToChange % 400 == 0) || ((yearToChange % 100 != 0) && (yearToChange % 4 == 0)))
                gui.setEditedTotDaysPerMonth(29);
            else
                gui.setEditedTotDaysPerMonth(28);
            }
            gui.removeCurrentDayButton();
            gui.updateMonthLabel();
            gui.disableAllButtons();
            gui.calendarTableEvents();
        }
        else {
            //do nothing because the user hit cancel
        }
    }
    
    /**
     * This method handles the calendar button click to add/edit/remove/view events 
     * for a certain day.
     * 
     * @param gui - the GUI
     * @param month - the month being viewed
     * @param button - the button the user pressed
     */
    public void handleDayEventsRequest(CAL_GUI gui, int month, Button button) {
        ded.showDayEventsDialog(month, gui.getEditedYear(), button);
        
        //after this, we're done because all the data has been taken care of in the DayEventsDialog.
    }
}