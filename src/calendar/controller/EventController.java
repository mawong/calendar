package calendar.controller;

import calendar.data.Event;
import static calendar.data.Event.COMPLETED_IN_PROGRESS;
import static calendar.data.Event.COMPLETED_NO;
import static calendar.data.Event.COMPLETED_YES;
import static calendar.data.Event.DEFAULT_YEAR;
import calendar.data.EventValues;
import static calendar.gui.AddEditRemoveEventDialog.PERIOD_AM;
import static calendar.gui.AddEditRemoveEventDialog.PERIOD_PM;
import calendar.gui.Alerts;
import calendar.gui.CAL_GUI;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class deals with all the events in the table/calendar.
 *
 * @author MAVIRI
 */
public class EventController {
    //the GUI!
    CAL_GUI gui;
    
    //the preset event values
    EventValues eventValues;
    
    //alert dialogs
    Alerts ad;
    
    //lists to hold the month's data
    ObservableList janList;
    ObservableList febList;
    ObservableList marList;
    ObservableList aprList;
    ObservableList mayList;
    ObservableList junList;
    ObservableList julList;
    ObservableList augList;
    ObservableList sepList;
    ObservableList octList;
    ObservableList novList;
    ObservableList decList;
    ObservableList viewMonthList;
    ObservableList viewDayList;
    
    //constants
    final String M_JANUARY = "JANUARY";
    final String M_FEBRUARY = "FEBRUARY";
    final String M_MARCH = "MARCH";
    final String M_APRIL = "APRIL";
    final String M_MAY = "MAY";
    final String M_JUNE = "JUNE";
    final String M_JULY = "JULY";
    final String M_AUGUST = "AUGUST";
    final String M_SEPTEMBER = "SEPTEMBER";
    final String M_OCTOBER = "OCTOBER";
    final String M_NOVEMBER = "NOVEMBER";
    final String M_DECEMBER = "DECEMBER";
    
    /**
     * Constructor that initializes everything -- the lists and the GUI/values
     * 
     * @param initGui 
     */
    public EventController(CAL_GUI initGui) {
        gui = initGui;
        eventValues = new EventValues(initGui);
        ad = new Alerts();
        
        //create the lists for the events
        janList = FXCollections.observableArrayList();
        febList = FXCollections.observableArrayList();
        marList = FXCollections.observableArrayList();
        aprList = FXCollections.observableArrayList();
        mayList = FXCollections.observableArrayList();
        junList = FXCollections.observableArrayList();
        julList = FXCollections.observableArrayList();
        augList = FXCollections.observableArrayList();
        sepList = FXCollections.observableArrayList();
        octList = FXCollections.observableArrayList();
        novList = FXCollections.observableArrayList();
        decList = FXCollections.observableArrayList();
        viewMonthList = FXCollections.observableArrayList();
        viewDayList = FXCollections.observableArrayList();
    }
    
    /**
     * This accessor method gets the event hash values.
     * 
     * @return the event hash values. Good for when we use loading, saving, and a new calendar
     */
    public EventValues getHashValues() {
        return eventValues;
    }
    
    /**
     * This accessor method gets the month list.
     * 
     * @return the month list
     */
    public ObservableList getMonthList() {
        return viewMonthList;
    }
    
    /**
     * This accessor method gets the day list.
     * 
     * @return the day list
     */
    public ObservableList getDayList() {
        return viewDayList;
    }
    
    /**
     * This method changes the list in the "month's events" table. It sets all the preset
     * data's dates, and keeps the non-preset data for us to use and the user to see.
     * 
     * @param month - the month the user is viewing
     */
    public void handleChangeMonthRequest(int month) {
        if (month == 1) {
            viewMonthList = eventValues.setJanuaryData(janList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 2) {
            viewMonthList = eventValues.setFebruaryData(febList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 3) {
            viewMonthList = eventValues.setMarchData(marList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 4) {
            viewMonthList = eventValues.setAprilData(aprList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 5) {
            viewMonthList = eventValues.setMayData(mayList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 6) {
            viewMonthList = eventValues.setJuneData(junList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 7) {
            viewMonthList = eventValues.setJulyData(julList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 8) {
            viewMonthList = eventValues.setAugustData(augList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 9) {
            viewMonthList = eventValues.setSeptemberData(sepList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 10) {
            viewMonthList = eventValues.setOctoberData(octList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else if (month == 11) {
            viewMonthList = eventValues.setNovemberData(novList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
        else {
            viewMonthList = eventValues.setDecemberData(decList);
            gui.setHolidayEventsCalendar(viewMonthList);
        }
    }
    
    /**
     * This method handles the "day's events" table -- the current day. It gets all the events
     * for the current day and returns them in a list.
     * 
     * @return the list of events for the current day
     */
    public ObservableList handleChangeDayRequest() {
        viewDayList.clear();
        int day = gui.getCurrentDay();
        int year = gui.getCurrentYear();
        ObservableList currentMonth = getListForCurrentMonth(gui.getCurrentMonth());
        
        for (Object list : currentMonth) {
            Event e = (Event)list;
            String[] date = e.getDate().split("/");
            int eYear = e.getYear();
            if (day == Integer.parseInt(date[1]) && (eYear == DEFAULT_YEAR || year == eYear))
                viewDayList.add(e);
        }
        
        return viewDayList;
    }
    
    /**
     * This method adds an event to the current month list (and adds it onto the month table).
     * 
     * @param e - the event to add
     */
    public void addEventToList(Event e) {
        ObservableList list = getListForCurrentMonth(gui.getEditedMonth());
        list.add(e);
        Collections.sort(list);
    }
    
    /**
     * This method clears all non-preset data. Usually for loading and creating a new calendar.
     */
    public void clearNonPresetData() {
        for (int i = 0; i < janList.size(); i++) {
            Object obj = janList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                janList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < febList.size(); i++) {
            Object obj = febList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                febList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < marList.size(); i++) {
            Object obj = marList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                marList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < aprList.size(); i++) {
            Object obj = aprList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                aprList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < mayList.size(); i++) {
            Object obj = mayList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                mayList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < junList.size(); i++) {
            Object obj = junList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                junList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < julList.size(); i++) {
            Object obj = julList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                julList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < augList.size(); i++) {
            Object obj = augList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                augList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < sepList.size(); i++) {
            Object obj = sepList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                sepList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < octList.size(); i++) {
            Object obj = octList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                octList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < novList.size(); i++) {
            Object obj = novList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                novList.remove(e);
                i--;
            }
        }
        for (int i = 0; i < decList.size(); i++) {
            Object obj = decList.get(i);
            Event e = (Event) obj;
            if (!e.getIsPreset()) {
                decList.remove(e);
                i--;
            }
        }
    }
    
    /**
     * This accessor method gets the month's list.
     * 
     * @param month - the month the user is viewing
     * @return the list of the month the user needs
     */
    public ObservableList getListForCurrentMonth(String month) {
        if (month.equals(M_JANUARY))
            return janList;
        else if (month.equals(M_FEBRUARY))
            return febList;
        else if (month.equals(M_MARCH))
            return marList;
        else if (month.equals(M_APRIL))
            return aprList;
        else if (month.equals(M_MAY))
            return mayList;
        else if (month.equals(M_JUNE))
            return junList;
        else if (month.equals(M_JULY))
            return julList;
        else if (month.equals(M_AUGUST))
            return augList;
        else if (month.equals(M_SEPTEMBER))
            return sepList;
        else if (month.equals(M_OCTOBER))
            return octList;
        else if (month.equals(M_NOVEMBER))
            return novList;
        else
            return decList;
    }
    
    /**
     * This method updates the day's events completed values. This is mainly for the today and month
     * table, and for events that are today. Used so that the calendar looks dynamic -- that it
     * changes every second.
     */
    public void updateTodayCompleted() {
        String period = PERIOD_AM;
        int hour = gui.getHour();
        if (hour == 0)
            hour = 12;
        else if (hour == 12)
            period = PERIOD_PM;
        else if (hour > 12) {
            hour -= 12;
            period = PERIOD_PM;
        }
        int minute = gui.getMinute();
        
        //go through each event for today (if any) and check if there are any values that need updates
        if (viewDayList.size() != 0) {
            for (Object obj : viewDayList) {
                Event e = (Event) obj;
                if (e.getCompleted().equals(COMPLETED_NO)) {
                    //first check start time
                    String[] getPeriod = e.getStartTime().split(" ");
                    if (period.equals(getPeriod[1])) {
                        String[] getHour = getPeriod[0].split(":");
                        if (hour == Integer.parseInt(getHour[0]) && minute == Integer.parseInt(getHour[1])) {
                            e.setCompleted(COMPLETED_IN_PROGRESS);
                            ad.EventStartAlert(e.getActivity());
                        }
                    }
                }
                else if (e.getCompleted().equals(COMPLETED_IN_PROGRESS)) {
                    //now check end time
                    String[] getPeriod = e.getEndTime().split(" ");
                    if (period.equals(getPeriod[1])) {
                        String[] getHour = getPeriod[0].split(":");
                        if (hour == Integer.parseInt(getHour[0]) && minute == Integer.parseInt(getHour[1])) {
                            e.setCompleted(COMPLETED_YES);
                        }
                    }
                }
            }
        }
    }
}