package calendar.controller;

import calendar.CAL_PropertyType;
import static calendar.CAL_StartupConstants.PATH_CALENDAR;
import calendar.error.ErrorHandler;
import calendar.file.CalendarFileManager;
import calendar.gui.CAL_GUI;
import calendar.gui.MessageDialog;
import calendar.gui.YesNoCancelDialog;
import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;

/**
 * This class controls all of the file options for the application.
 * 
 * @author MAVIRI
 */
public class FileController {
    //keep track of when the calendar has been saved
    private boolean saved;
    
    //read and write data
    private final CalendarFileManager calendarIO;
    
    //MessageDialog for feedback
    MessageDialog messageDialog;
    
    //YesNoCancelDialog for feedback
    YesNoCancelDialog yncDialog;
    
    //ErrorHandler for feedback
    ErrorHandler errorHandler;
    
    //get the properties for feedback
    PropertiesManager props;
    
    /**
     * Constructor that creates the file controller and initializes all the necessary items
     * 
     * @param initMD - MessageDialog
     * @param initYNCD - YesNoCancelDialog
     * @param initIO - CalendarFileManager
     */
    public FileController(MessageDialog initMD, YesNoCancelDialog initYNCD, CalendarFileManager initIO) {
        //nothing yet
        saved = true;
        messageDialog = initMD;
        yncDialog = initYNCD;
        calendarIO = initIO;
        props = PropertiesManager.getPropertiesManager();
        errorHandler = ErrorHandler.getErrorHandler();
    }
    
    /**
     * This method marks the calendar as edited, so we may need to save
     * 
     * @param gui - The GUI to be saved.
     */
    public void markAsEdited(CAL_GUI gui) {
        //make saved false
        saved = false;
        
        //and update the saved button
        gui.updateToolbarControls(saved);
    }
    
    /**
     * This method deals with making a fresh, new calendar. If we need to save the previous
     * calendar, we will prompt the user to save first. The new calendar is similar to the
     * load calendar, except we do not add anything into the non-preset events lists.
     * 
     * @param gui - the GUI we will edit
     */
    public void handleNewCalendarRequest(CAL_GUI gui) {
        try {
            boolean continueToMakeNew = true;
            if (!saved)
                continueToMakeNew = promptToSave(gui);
            
            if (continueToMakeNew) {
                gui.setCalendarName("");
                gui.getHashController().getHashValues().clearAllLists();
                gui.getHashController().clearNonPresetData();
                gui.setEditedMonth(gui.getCurrentMonth());
                gui.setEditedTotDaysPerMonth(gui.getCurrentTotDaysPerMonth());
                gui.setEditedYear(gui.getCurrentYear());
                gui.setIntEditedMonth(gui.getIntCurrentMonth());
                gui.removeCurrentDayButton();
                gui.updateMonthLabel();
                gui.disableAllButtons();
                gui.calendarTableEvents();
                gui.setTheTables();
            }
        } catch (IOException ioe) {
            errorHandler.handleNewCalendarError();
        }
    }
    
    /**
     * This method deals with saving the calendar events, if the user would like to
     * save. If the filename (name of calendar) is empty, we will not save the calendar.
     * 
     * @param gui - The  GUI stuff to be saved
     */
    public void handleSaveCalendarRequest(CAL_GUI gui) {
        try {
            //save it to a file
            if (gui.getCalendarName() == null || gui.getCalendarName().equals(""))
                messageDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_NAME_EMPTY_MESSAGE));
            else {
                calendarIO.saveCalendar(gui);
                saved = true;
                messageDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_SAVED_MESSAGE));
                gui.updateToolbarControls(saved);
            }
        } catch (IOException ioe) {
            errorHandler.handleSaveCalendarError();
        }
    }
    
    /**
     * This method deals with loading a pre-existing calendar to the GUI. If we need to save the
     * previous calendar before moving on, we will prompt the user to save before bringing up the
     * loading screen.
     * 
     * @param gui - the GUI stuff to be edited
     */
    public void handleLoadCalendarRequest(CAL_GUI gui) {
        try {
            //do we have to save?
            boolean continueToOpen = true;
            if (!saved)
                continueToOpen = promptToSave(gui);
            
            if (continueToOpen) {
                //ask the user which one to open
                promptToOpen(gui);
            }
        } catch (IOException ioe) {
            errorHandler.handleLoadCalendarError();
        }
    }
    
    /**
     * This method safely ends the program. If the current calendar needs to be saved, we will
     * prompt the user to save before exiting the application.
     * 
     * @param gui - The GUI for the program
     */
    public void handleExitRequest(CAL_GUI gui) {
        try {
            //do we need to save?
            boolean continueToExit = true;
            if (!saved) {
                //prompt this, so the user can opt out
                continueToExit = promptToSave(gui);
            }
            
            //user wants to exit the app
            if (continueToExit) {
                //stops the thread and ends the program
                gui.setRunningFalse();
                System.exit(0);
            }
        } catch(IOException ioe) {
            errorHandler.handleExitError();
        }
    }
    
    /**
     * This method asks the user for a file to open. The user-selected file is loaded and 
     * the GUI updated. If the user cancels the open process, nothing is done. If an error 
     * occurs loading the file, a message is displayed and nothing happens.
     */
    private void promptToOpen(CAL_GUI gui) {
        //ask the user for the calendar to open
        FileChooser courseFileChooser = new FileChooser();
        courseFileChooser.setInitialDirectory(new File(PATH_CALENDAR));
        File selectedFile = courseFileChooser.showOpenDialog(gui.getWindow());

        //only  open if the user clicks OK
        if (selectedFile != null) {
            try {
                calendarIO.loadCalendar(gui, selectedFile.getAbsolutePath());
                saved = true;
                gui.updateToolbarControls(saved);
            } catch (Exception e) {
                errorHandler.handleLoadCalendarError();
            }
        }
    }
    
    /**
     * This method verifies that the user really wants to save the calendar if the work is
     * unsaved. YES means the user wants to save their work and continue the other action 
     * (return true), NO means don't save the work but continue with the other action (return true),
     * CANCEL means don't save the work and don't continue with the other action (return false).
     *
     * @return true if the user presses the YES option to save, true if the user
     *         presses the NO option to not save, false if the user presses the CANCEL
     *         option to not continue.
     */
    private boolean promptToSave(CAL_GUI gui) throws IOException {
        //prompt the user to save
        yncDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_SAVE_UNSAVED_WORK_MESSAGE));
        
        //get the selection
        String selection = yncDialog.getSelection();

        //if yes, save and move on
        if (selection.equals(YesNoCancelDialog.YES)) {
            //save the calendar events
            calendarIO.saveCalendar(gui);
            saved = true;
            
        } //if cancel, do nothing and move on
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        //if no, do nothing AND return true, so we can move on
        return true;
    }
}