package calendar.data;

import java.util.Comparator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a single event object. We will place these events on a table,
 * and save these events when necessary. Note: saving only pertains to non-preset events.
 *
 * @author MAVIRI
 */
public class Event implements Comparable, Comparator<Event> {
    final StringProperty date;
    final StringProperty activity;
    final StringProperty completed;
    final StringProperty time;
    final StringProperty timeEnd;
    boolean isHoliday;
    boolean isPreset;
    int year;
    public static final String DEFAULT_DATE = "1/1";
    public static final String DEFAULT_ACTIVITY = "N/A";
    public static final String COMPLETED_NO = "No";
    public static final String COMPLETED_IN_PROGRESS = "In Progress";
    public static final String COMPLETED_YES = "Yes";
    public static final String DEFAULT_TIME = "12:00 AM";
    public static final String TIME_AM = "AM";
    public static final int DEFAULT_YEAR = -1;
    public static final String DEFAULT_TIME_END = "11:59 PM";
    
    /**
     * Constructor that creates an event and sets its default values for the table.
     */
    public Event() {
        date = new SimpleStringProperty(DEFAULT_DATE);
        activity = new SimpleStringProperty(DEFAULT_ACTIVITY);
        completed = new SimpleStringProperty(COMPLETED_NO);
        time = new SimpleStringProperty(DEFAULT_TIME);
        timeEnd = new SimpleStringProperty(DEFAULT_TIME_END);
        isHoliday = false;
        isPreset = false;
        year = DEFAULT_YEAR;
    }
    
    /**
     * This accessor method gets the event's date.
     * 
     * @return the event's date
     */
    public String getDate() {
        return date.get();
    }
    
    /**
     * This mutator method sets the event's date.
     * 
     * @param newDate - the event's date
     */
    public void setDate(String newDate) {
        date.set(newDate);
    }
    
    /**
     * This method returns the date property (for the table).
     * 
     * @return the date property (case-sensitive)
     */
    public StringProperty dateProperty() {
        return date;
    }
    
    /**
     * This method gets the event's activity.
     * 
     * @return the event's activity
     */
    public String getActivity() {
        return activity.get();
    }
    
    /**
     * This method sets the event's activity.
     * 
     * @param newActivity - the event's activity
     */
    public void setActivity(String newActivity) {
        activity.set(newActivity);
    }
    
    /**
     * This method returns the activity property.
     * 
     * @return the activity property (case-sensitive)
     */
    public StringProperty activityProperty() {
        return activity;
    }
    
    /**
     * This method gets the event's completed status.
     * 
     * @return the event's completed status
     */
    public String getCompleted() {
        return completed.get();
    }
    
    /**
     * This method sets the event's completed status
     * 
     * @param newCompleted - the event's completed status
     */
    public void setCompleted(String newCompleted) {
        completed.set(newCompleted);
    }
    
    /**
     * This method returns the event's completed status property.
     * 
     * @return the event's completed status property (case-sensitive)
     */
    public StringProperty completedProperty() {
        return completed;
    }
    
    /**
     * This method gets the event's start time.
     * 
     * @return the event's start time
     */
    public String getStartTime() {
        return time.get();
    }
    
    /**
     * This method sets the event's start time.
     * 
     * @param newTime - the event's start time
     */
    public void setStartTime(String newTime) {
        time.set(newTime);
    }
    
    /**
     * This method returns the event's start time property.
     * 
     * @return the event's start time property (case-sensitive)
     */
    public StringProperty starttimeProperty() {
        return time;
    }
    
    /**
     * This method gets the isHoliday value.
     * 
     * @return the isHoliday value
     */
    public boolean getIsHoliday() {
        return isHoliday;
    }
    
    /**
     * This method sets the isHoliday value.
     * 
     * @param isIt - the isHoliday value
     */
    public void setIsHoliday(boolean isIt) {
        isHoliday = isIt;
    }
    
    /**
     * This method gets the isPreset value.
     * 
     * @return the isPreset value
     */
    public boolean getIsPreset() {
        return isPreset;
    }
    
    /**
     * This method sets the isPreset value.
     * 
     * @param isIt the isPresetValue
     */
    public void setIsPreset(boolean isIt) {
        isPreset = isIt;
    }
    
    /**
     * This method gets the event's year.
     * 
     * @return the event's year
     */
    public int getYear() {
        return year;
    }
    
    /**
     * This method sets the event's year.
     * 
     * @param initYear - the event's year
     */
    public void setYear(int initYear) {
        year = initYear;
    }
    
    /**
     * This method gets the event's end time.
     * 
     * @return the event's end time
     */
    public String getEndTime() {
        return timeEnd.get();
    }
    
    /**
     * This method sets the event's end time.
     * 
     * @param endTime - the event's end time
     */
    public void setEndTime(String endTime) {
        timeEnd.set(endTime);
    }
    
    /**
     * This method returns the event's start time property.
     * 
     * @return the event's start time property (case-sensitive)
     */
    public StringProperty endtimeProperty() {
        return timeEnd;
    }

    /**
     * This method compares two events based on its date. Used mainly for the month's table.
     * If the dates are the same, calls another comparator method that compares the events
     * based on its time.
     * 
     * @param obj - the other event
     * @return 1 if this event's date is greater than the other, -1 if this event's date is less
     *         than the other, or 0 if they occur at the same time
     */
    @Override
    public int compareTo(Object obj) {
        Event otherEvent = (Event)obj;
        String thisDate = this.getDate();
        String otherDate = otherEvent.getDate();
        String[] thisDay = thisDate.split("/");
        String[] otherDay = otherDate.split("/");
        int intThisDate = Integer.parseInt(thisDay[1]);
        int intOtherDate = Integer.parseInt(otherDay[1]);
        if (intThisDate > intOtherDate)
            return 1;
        else if (intThisDate < intOtherDate)
            return -1;
        else
            return compare(this, otherEvent);
    }

    /**
     * This method compares to events by checking for its start time.
     * 
     * @param o1 - the first event
     * @param o2 - the second event
     * @return 1 if the first event's start time is greater than the second event's, -1 if
     *         the first event's start time is less than the second event's, and 0 if they 
     *         are the same.
     */
    @Override
    public int compare(Event o1, Event o2) {
        int hour1;
        int minute1;
        int hour2;
        int minute2;
        
        String[] get1Hour = o1.getStartTime().split(":");        //hour[0] is the hour
        String[] get1Minute = get1Hour[1].split(" ");       //minute[0] is the minute
        String convert1ToMilitary = get1Minute[1];          //"12:00 AM" hour[0] = 12, minute[0] = 0
        if (convert1ToMilitary.equals(TIME_AM)) {
            if (Integer.parseInt(get1Hour[0]) == 12)
                    hour1 = 0;
            else
                hour1 = Integer.parseInt(get1Hour[0]);
        }
        else {
            if (!(Integer.parseInt(get1Hour[0]) == 12))
                hour1 = 12 + Integer.parseInt(get1Hour[0]);
            else
                hour1 = Integer.parseInt(get1Hour[0]);
        }
        minute1 = Integer.parseInt(get1Minute[0]);
        
        String[] get2Hour = o2.getStartTime().split(":");        //hour[0] is the hour
        String[] get2Minute = get2Hour[1].split(" ");       //minute[0] is the minute
        String convert2ToMilitary = get2Minute[1];          //"12:00 AM" hour[0] = 12, minute[0] = 0
        if (convert2ToMilitary.equals(TIME_AM)) {
            if (Integer.parseInt(get2Hour[0]) == 12)
                    hour2 = 0;
            else
                hour2 = Integer.parseInt(get2Hour[0]);
        }
        else {
            if (!(Integer.parseInt(get2Hour[0]) == 12))
                hour2 = 12 + Integer.parseInt(get2Hour[0]);
            else
                hour2 = Integer.parseInt(get2Hour[0]);
        }
        minute2 = Integer.parseInt(get2Minute[0]);
        
        if (hour1 > hour2)
            return 1;
        else if (hour1 < hour2)
            return -1;
        else {
            if (minute1 > minute2)
                return 1;
            else if (minute1 < minute2)
                return -1;
            else
                return 0;
        }
    }
}