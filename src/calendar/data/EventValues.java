package calendar.data;

import static calendar.data.Event.COMPLETED_IN_PROGRESS;
import static calendar.data.Event.COMPLETED_NO;
import static calendar.data.Event.COMPLETED_YES;
import static calendar.gui.AddEditRemoveEventDialog.PERIOD_AM;
import static calendar.gui.AddEditRemoveEventDialog.PERIOD_PM;
import calendar.gui.CAL_GUI;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class contains all the event values for each list; loads all the important
 * dates for each month. Note: we get the lists from which to load the values in from
 * the GUI.
 *
 * @author MAVIRI
 */
public class EventValues {
    //GUI stuff
    CAL_GUI gui;
    
    //constants; preset data to be placed into the respective lists
    public static final String JAN_NEW_YEARS_DAY = "New Year's Day";                        //always 1/1
    public static final String JAN_MARTIN_LUTHER_KING_DAY = "Martin Luther King, JR. Day";  //always 3rd Mon
    public static final String FEB_GROUNDHOG_DAY = "Groundhog Day";                         //always 2/2
    public static final String FEB_VALENTINES_DAY = "Valentine's Day";                      //always 2/14
    public static final String FEB_PRESIDENTS_DAY = "Presidents' Day";                      //always 3rd Mon
    public static final String MAR_INTERNATIONAL_WOMENS_DAY = "International Women's Day";  //always 3/8
    public static final String MAR_ST_PATRICKS_DAY = "St. Patrick's Day";                   //always 3/17
    public static final String MAR_SPRING_BEGINS = "Spring Begins";                         //always 3/20
    public static final String APR_HEALTH_DAY = "World Health Day";                         //always 4/7
    public static final String APR_TAX_DAY = "Tax Day";                                     //always 4/15 -- Weekend = Next Mon
    public static final String APR_EARTH_DAY = "International Earth Day";                   //always 4/22
    public static final String APR_ARBOR_DAY = "Arbor Day";                                 //always last Fri
    public static final String MAY_MAY_DAY = "May Day";                                     //always 5/1
    public static final String MAY_MOTHERS_DAY = "Mother's Day";                            //always 2nd Sun
    public static final String MAY_ARMED_FORCES_DAY = "Armed Forces Day";                   //always 3rd Sat
    public static final String MAY_MEMORIAL_DAY = "Memorial Day";                           //always last Mon
    public static final String JUN_FLAG_DAY = "Flag Day";                                   //always 6/14
    public static final String JUN_FATHERS_DAY = "Father's Day";                            //always 3rd Sun
    public static final String JUN_SUMMER_BEGINS = "Summer Begins";                         //6/20 if year % 4; 6/21 else
    public static final String JUL_INDEPENDENCE_DAY = "Independence Day";                   //always 7/4
    public static final String JUL_BASTILLE_DAY = "Bastille Day (French Independence)";     //always 7/14
    public static final String AUG_FRIENDSHIP_DAY = "International Friendship Day";         //always 1st Sun
    public static final String AUG_LEFT_HAND_DAY = "International Left-Handers Day";        //always 8/13
    public static final String SEP_LABOR_DAY = "Labor Day";                                 //always 1st Mon
    public static final String SEP_PATRIOT_DAY = "Patriot Day";                             //always 9/11
    public static final String SEP_MEX_IND_DAY = "Grito de Dolores (Mexican Independence)"; //always 9/16
    public static final String SEP_AUTUMN_BEGINS = "Fall Begins";                           //9/22 if year % 4 == 0|1; 9/23 else
    public static final String OCT_COLUMBUS_DAY = "Columbus Day";                           //always 2nd Mon
    public static final String OCT_VEGETARIAN_DAY = "World Vegetarian Day";                 //always 10/1
    public static final String OCT_HALLOWEEN_DAY = "Halloween";                             //always 10/31
    public static final String NOV_ELECTION_DAY = "Election Day";                           //always Tue after 1st Mon
    public static final String NOV_VETERANS_DAY = "Veterans Day";                           //always 11/11
    public static final String NOV_THANKSGIVING_DAY = "Thanksgiving";                       //always 4th Thu
    public static final String DEC_HUMAN_RIGHTS_DAY = "Human Rights Day";                   //always 12/10
    public static final String DEC_WINTER_BEGINS = "Winter Begins";                         //12/22 if year % 4 == 3; 12/21 else
    public static final String DEC_CHRISTMAS_DAY = "Christmas";                             //always 12/25
    public static final String DEC_NEW_YEARS_EVE_DAY = "New Year's Eve";                    //always 12/31
    
    //events
    Event janNewYears, janMLK;
    Event febGroundhog, febValentine, febPresidents;
    Event marWomens, marStPat, marSpring;
    Event aprHealth, aprTax, aprEarth, aprArbor;
    Event mayMay, mayMothers, mayArmedForces, mayMemorial;
    Event junFlag, junFathers, junSummer;
    Event julIndependence, julBastille;
    Event augFriendship, augLeftHand;
    Event sepLabor, sepPatriot, sepMexInd, sepFall;
    Event octColumbus, octVegetarian, octHalloween;
    Event novElection, novVeterans, novThanksgiving;
    Event decHRD, decWinter, decChristmas, decNYE;
    
    //list of events made during runtime that are not in the preset list
    ObservableList newEvents;
    ObservableList eventsInUse;
    ObservableList allNewEvents;
    
    /**
     * Constructor that creates the preset events and adds them to the GUI.
     * 
     * @param newGUI - the GUI
     */
    public EventValues(CAL_GUI newGUI) {
        gui = newGUI;
        
        newEvents = FXCollections.observableArrayList();
        eventsInUse = FXCollections.observableArrayList();
        allNewEvents = FXCollections.observableArrayList();
                
        janNewYears = createPresetEvent(JAN_NEW_YEARS_DAY, "No", true);
        janMLK = createPresetEvent(JAN_MARTIN_LUTHER_KING_DAY, "No", true);
        
        febGroundhog = createPresetEvent(FEB_GROUNDHOG_DAY, "2/2", false);
        febValentine = createPresetEvent(FEB_VALENTINES_DAY, "2/14", false);
        febPresidents = createPresetEvent(FEB_PRESIDENTS_DAY, "No", true);
        
        marWomens = createPresetEvent(MAR_INTERNATIONAL_WOMENS_DAY, "3/8", false);
        marStPat = createPresetEvent(MAR_ST_PATRICKS_DAY, "3/17", false);
        marSpring = createPresetEvent(MAR_SPRING_BEGINS, "No", false);
        
        aprHealth = createPresetEvent(APR_HEALTH_DAY, "4/7", false);
        aprTax = createPresetEvent(APR_TAX_DAY, "No", false);
        aprEarth = createPresetEvent(APR_EARTH_DAY, "4/22", false);
        aprArbor = createPresetEvent(APR_ARBOR_DAY, "No", false);
        
        mayMay = createPresetEvent(MAY_MAY_DAY, "5/1", false);
        mayMothers = createPresetEvent(MAY_MOTHERS_DAY, "No", true);
        mayArmedForces = createPresetEvent(MAY_ARMED_FORCES_DAY, "No", true);
        mayMemorial = createPresetEvent(MAY_MEMORIAL_DAY, "No", true);
        
        junFlag = createPresetEvent(JUN_FLAG_DAY, "6/14", false);
        junFathers = createPresetEvent(JUN_FATHERS_DAY,"No", true);
        junSummer = createPresetEvent(JUN_SUMMER_BEGINS, "No", false);
        
        julIndependence = createPresetEvent(JUL_INDEPENDENCE_DAY, "7/4", true);
        julBastille = createPresetEvent(JUL_BASTILLE_DAY, "7/14", true);
        
        augFriendship = createPresetEvent(AUG_FRIENDSHIP_DAY, "No", false);
        augLeftHand = createPresetEvent(AUG_LEFT_HAND_DAY, "8/13", false);
        
        sepLabor = createPresetEvent(SEP_LABOR_DAY, "No", true);
        sepPatriot = createPresetEvent(SEP_PATRIOT_DAY, "9/11", false);
        sepMexInd = createPresetEvent(SEP_MEX_IND_DAY, "9/16", true);
        sepFall = createPresetEvent(SEP_AUTUMN_BEGINS, "No", false);
        
        octColumbus = createPresetEvent(OCT_COLUMBUS_DAY, "No", true);
        octVegetarian = createPresetEvent(OCT_VEGETARIAN_DAY, "10/1", false);
        octHalloween = createPresetEvent(OCT_HALLOWEEN_DAY, "10/31", false);
        
        novElection = createPresetEvent(NOV_ELECTION_DAY, "No", true);
        novVeterans = createPresetEvent(NOV_VETERANS_DAY, "11/11", true);
        novThanksgiving = createPresetEvent(NOV_THANKSGIVING_DAY, "No", true);
        
        decHRD = createPresetEvent(DEC_HUMAN_RIGHTS_DAY, "12/10", false);
        decWinter = createPresetEvent(DEC_WINTER_BEGINS, "No", false);
        decChristmas = createPresetEvent(DEC_CHRISTMAS_DAY, "12/25", true);
        decNYE = createPresetEvent(DEC_NEW_YEARS_EVE_DAY, "12/31", true);
    }
    
    /**
     * This method places January's data into an observable list.
     * 
     * @param janList - the list
     * @return the list
     */
    public ObservableList setJanuaryData(ObservableList janList) {
        for (int i = 0; i < janList.size(); i++) {
            Event e = (Event)janList.get(i);
            if (e.getIsPreset()) {
                janList.remove(e);
                i--;
            }
            else {
                if (gui.getEditedYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    janList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(janNewYears);
        janList.add(janNewYears);
        janMLK.setDate("1/" + gui.getDateFromKnownButton(3, 1));
        setCompletedEvents(janMLK);
        janList.add(janMLK);
        getNewEventsList(janList);
        Collections.sort(janList);
        return janList;
    }
    
    /**
     * This method places February's data into an observable list.
     * 
     * @param febList - the list
     * @return the list
     */
    public ObservableList setFebruaryData(ObservableList febList) {
        for (int i = 0; i < febList.size(); i++) {
            Event e = (Event)febList.get(i);
            if (e.getIsPreset()) {
                febList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    febList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(febGroundhog);
        febList.add(febGroundhog);
        setCompletedEvents(febValentine);
        febList.add(febValentine);
        febPresidents.setDate("2/" + gui.getDateFromKnownButton(3, 1));
        setCompletedEvents(febPresidents);
        febList.add(febPresidents);
        getNewEventsList(febList);
        Collections.sort(febList);
        return febList;
    }
    
    /**
     * This method places March's data into an observable list.
     * 
     * @param marList - the list
     * @return the list
     */
    public ObservableList setMarchData(ObservableList marList) {
        for (int i = 0; i < marList.size(); i++) {
            Event e = (Event)marList.get(i);
            if (e.getIsPreset()) {
                marList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    marList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(marWomens);
        marList.add(marWomens);
        setCompletedEvents(marStPat);
        marList.add(marStPat);
        marSpring.setDate(gui.getChangeSeasons(0));
        setCompletedEvents(marSpring);
        marList.add(marSpring);
        getNewEventsList(marList);
        Collections.sort(marList);
        return marList;
    }
    
    /**
     * This method places April's data into an observable list.
     * 
     * @param aprList - the list
     * @return the list
     */
    public ObservableList setAprilData(ObservableList aprList) {
        for (int i = 0; i < aprList.size(); i++) {
            Event e = (Event)aprList.get(i);
            if (e.getIsPreset()) {
                aprList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    aprList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(aprHealth);
        aprList.add(aprHealth);
        aprTax.setDate("4/" + gui.getButtonForTaxDay());
        setCompletedEvents(aprTax);
        aprList.add(aprTax);
        setCompletedEvents(aprEarth);
        aprList.add(aprEarth);
        aprArbor.setDate("4/" + gui.getLastDateFromKnownButton(5));
        setCompletedEvents(aprArbor);
        aprList.add(aprArbor);
        getNewEventsList(aprList);
        Collections.sort(aprList);
        return aprList;
    }
    
    /**
     * This method places May's data into an observable list.
     * 
     * @param mayList - the list
     * @return the list
     */
    public ObservableList setMayData(ObservableList mayList) {
        for (int i = 0; i < mayList.size(); i++) {
            Event e = (Event)mayList.get(i);
            if (e.getIsPreset()) {
                mayList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    mayList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(mayMay);
        mayList.add(mayMay);
        mayMothers.setDate("5/" + gui.getDateFromKnownButton(2, 0));
        setCompletedEvents(mayMothers);
        mayList.add(mayMothers);
        mayArmedForces.setDate("5/" + gui.getDateFromKnownButton(3, 6));
        setCompletedEvents(mayArmedForces);
        mayList.add(mayArmedForces);
        mayMemorial.setDate("5/" + gui.getLastDateFromKnownButton(1));
        setCompletedEvents(mayMemorial);
        mayList.add(mayMemorial);
        getNewEventsList(mayList);
        Collections.sort(mayList);
        return mayList;
    }
    
    /**
     * This method places June's data into an observable list.
     * 
     * @param junList - the list
     * @return the list
     */
    public ObservableList setJuneData(ObservableList junList) {
        for (int i = 0; i < junList.size(); i++) {
            Event e = (Event)junList.get(i);
            if (e.getIsPreset()) {
                junList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    junList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(junFlag);
        junList.add(junFlag);
        junFathers.setDate("6/" + gui.getDateFromKnownButton(3, 0));
        setCompletedEvents(junFathers);
        junList.add(junFathers);
        junSummer.setDate(gui.getChangeSeasons(1));
        setCompletedEvents(junSummer);
        junList.add(junSummer);
        getNewEventsList(junList);
        Collections.sort(junList);
        return junList;
    }
    
    /**
     * This method places July's data into an observable list.
     * 
     * @param julList - the list
     * @return the list
     */
    public ObservableList setJulyData(ObservableList julList) {
        for (int i = 0; i < julList.size(); i++) {
            Event e = (Event)julList.get(i);
            if (e.getIsPreset()) {
                julList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    julList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(julIndependence);
        julList.add(julIndependence);
        setCompletedEvents(julBastille);
        julList.add(julBastille);
        getNewEventsList(julList);
        Collections.sort(julList);
        return julList;
    }
    
    /**
     * This method places August's data into an observable list.
     * 
     * @param augList - the list
     * @return the list
     */
    public ObservableList setAugustData(ObservableList augList) {
        for (int i = 0; i < augList.size(); i++) {
            Event e = (Event)augList.get(i);
            if (e.getIsPreset()) {
                augList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    augList.remove(e);
                    i--;
                }
            }
        }
        augFriendship.setDate("8/" + gui.getDateFromKnownButton(1, 0));
        setCompletedEvents(augFriendship);
        augList.add(augFriendship);
        setCompletedEvents(augLeftHand);
        augList.add(augLeftHand);
        getNewEventsList(augList);
        Collections.sort(augList);
        return augList;
    }
    
    /**
     * This method places September's data into an observable list.
     * 
     * @param sepList - the list
     * @return the list
     */
    public ObservableList setSeptemberData(ObservableList sepList) {
        for (int i = 0; i < sepList.size(); i++) {
            Event e = (Event)sepList.get(i);
            if (e.getIsPreset()) {
                sepList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    sepList.remove(e);
                    i--;
                }
            }
        }
        sepLabor.setDate("9/" + gui.getDateFromKnownButton(1, 1));
        setCompletedEvents(sepLabor);
        sepList.add(sepLabor);
        setCompletedEvents(sepPatriot);
        sepList.add(sepPatriot);
        setCompletedEvents(sepMexInd);
        sepList.add(sepMexInd);
        sepFall.setDate(gui.getChangeSeasons(2));
        setCompletedEvents(sepFall);
        sepList.add(sepFall);
        getNewEventsList(sepList);
        Collections.sort(sepList);
        return sepList;
    }
    
    /**
     * This method places October's data into an observable list.
     * 
     * @param octList - the list
     * @return the list
     */
    public ObservableList setOctoberData(ObservableList octList) {
        for (int i = 0; i < octList.size(); i++) {
            Event e = (Event)octList.get(i);
            if (e.getIsPreset()) {
                octList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    octList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(octVegetarian);
        octList.add(octVegetarian);
        octColumbus.setDate("10/" + gui.getDateFromKnownButton(2, 1));
        setCompletedEvents(octColumbus);
        octList.add(octColumbus);
        setCompletedEvents(octHalloween);
        octList.add(octHalloween);
        getNewEventsList(octList);
        Collections.sort(octList);
        return octList;
    }
    
    /**
     * This method places November's data into an observable list.
     * 
     * @param novList - the list
     * @return the list
     */
    public ObservableList setNovemberData(ObservableList novList) {
        for (int i = 0; i < novList.size(); i++) {
            Event e = (Event)novList.get(i);
            if (e.getIsPreset()) {
                novList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    novList.remove(e);
                    i--;
                }
            }
        }
        novElection.setDate("11/" + gui.getButtonForElectionDay());
        setCompletedEvents(novElection);
        novList.add(novElection);
        setCompletedEvents(novVeterans);
        novList.add(novVeterans);
        novThanksgiving.setDate("11/" + gui.getDateFromKnownButton(4, 4));
        setCompletedEvents(novThanksgiving);
        novList.add(novThanksgiving);
        getNewEventsList(novList);
        Collections.sort(novList);
        return novList;
    }
    
    /**
     * This method places December's data into an observable list.
     * 
     * @param decList - the list
     * @return the list
     */
    public ObservableList setDecemberData(ObservableList decList) {
        for (int i = 0; i < decList.size(); i++) {
            Event e = (Event)decList.get(i);
            if (e.getIsPreset()) {
                decList.remove(e);
                i--;
            }
            else {
                if (gui.getCurrentYear() != e.getYear()) {
                    newEvents.add(e);
                    eventsInUse.remove(e);
                    decList.remove(e);
                    i--;
                }
            }
        }
        setCompletedEvents(decHRD);
        decList.add(decHRD);
        decWinter.setDate(gui.getChangeSeasons(3));
        setCompletedEvents(decWinter);
        decList.add(decWinter);
        setCompletedEvents(decChristmas);
        decList.add(decChristmas);
        setCompletedEvents(decNYE);
        decList.add(decNYE);
        getNewEventsList(decList);
        Collections.sort(decList);
        return decList;
    }
    /**
     * This method creates a preset event, and saves space while doing it.
     * 
     * @param activity - the event's activity
     * @param date - the event's date (if any; if "No", it means the date's determined later)
     * @param isHoliday - true if the event is a holiday, false otherwise
     * @return the event
     */
    private Event createPresetEvent(String activity, String date, boolean isHoliday) {
        Event e = new Event();
        e.setActivity(activity);
        e.setIsHoliday(isHoliday);
        e.setIsPreset(true);
        if (!date.equals("No"))
            e.setDate(date);
        return e;
    }
    
    /**
     * This method gets the list of new events made.
     * 
     * @return the list of new events made
     */
    public ObservableList getAllNewEvents() {
        return allNewEvents;
    }
    
    /**
     * This method clears the list of the new events. Mainly for loading and creating
     * a new calendar.
     */
    public void clearAllLists() {
        allNewEvents.clear();
        eventsInUse.clear();
        newEvents.clear();
    }
    
    /**
     * This method sets the list of new events made. Mainly for loading a calendar.
     * 
     * @param list - the list of new events made
     */
    public void setNewEventsLists(ObservableList list) {
        for (Object obj : list) {
            Event e = (Event) obj;
            e.setCompleted(setEventCompleted(e, e.getStartTime(), e.getEndTime()));
            allNewEvents.add(e);
            newEvents.add(e);
        }
    }
    
    /**
     * This method sets the completed event variable. This is ONLY for preset data, so we don't
     * check the event's time here. Note: the time is always 12:00 AM, so we don't care about it.
     * 
     * @param e - the event we are setting to be complete
     */
    private void setCompletedEvents(Event e) {
        if (gui.getEditedYear() > gui.getCurrentYear())         //since the year we're looking at is greater
            e.setCompleted(COMPLETED_NO);                       //we're in the future
        else if (gui.getEditedYear() < gui.getCurrentYear())    //similarly to above, we're in the past
            e.setCompleted(COMPLETED_YES);
        else {                                                  //check months, then days
            if (gui.getIntEditedMonth() > gui.getMonth(gui.getCurrentMonth()))          //future
                e.setCompleted(COMPLETED_NO);
            else if (gui.getIntEditedMonth() < gui.getMonth(gui.getCurrentMonth()))     //past
                e.setCompleted(COMPLETED_YES);
            else {                                              //same year, same month
                String[] getDate = e.getDate().split("/");
                int date = Integer.parseInt(getDate[1]);
                if (date > gui.getCurrentDay())         //future
                    e.setCompleted(COMPLETED_NO);
                else if (date < gui.getCurrentDay())    //past
                    e.setCompleted(COMPLETED_YES);
                else                                    //same day
                    e.setCompleted(COMPLETED_IN_PROGRESS);
            }
        }
    }
    
    /**
     * This method checks if the event is already completed (Yes, No, or In Progress). Here, we DO
     * worry about the time.
     * 
     * @param event - the event we are setting
     * @param startTime - the event's start time
     * @param endTime - the event's end time
     * @return "Yes", "No", or "In Progress", depending on its times and the current date
     */
    private String setEventCompleted(Event event, String startTime, String endTime) {
        //first, check if the year and days are good
        setCompletedEvents(event);
        
        if (event.getCompleted().equals(COMPLETED_IN_PROGRESS)) {
            String[] endTimeAndPeriod = endTime.split(" ");
            if ((endTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_AM)) || (endTimeAndPeriod[1].equals(PERIOD_PM) && gui.getPeriod().equals(PERIOD_PM))) {
                String[] endHRandMIN = endTimeAndPeriod[0].split(":");
                int hour = Integer.parseInt(endHRandMIN[0]);
                int guiHour = gui.getHour();
                if (guiHour == 0)
                    guiHour = 12;
                else if (guiHour > 12)
                    guiHour -= 12;
                if (hour < guiHour || (hour == 12 && hour > guiHour))
                    return "Yes";
                else if (hour == guiHour) {
                    int min = Integer.parseInt(endHRandMIN[1]);
                    int guiMin = gui.getMinute();
                    if (min <= guiMin)
                        return "Yes";
                    else {
                        return getStartTime(startTime);
                    }
                }
                else
                    return getStartTime(startTime);
            }
            else if ((endTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_PM)))
                return "Yes";
            else
                return getStartTime(startTime);
        }
        else
            return event.getCompleted();
    }
    
    /**
     * This method helps set the completed event (Yes, No, or In Progress) by looking at its start time.
     * 
     * @param startTime - the event's start time
     * @return "Yes", "No", or "In Progress", depending on its time and the current date
     */
    private String getStartTime(String startTime) {
        String[] startTimeAndPeriod = startTime.split(" ");
        if ((startTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_AM)) || (startTimeAndPeriod[1].equals(PERIOD_PM) && gui.getPeriod().equals(PERIOD_PM))) {
            String[] startHRandMIN = startTimeAndPeriod[0].split(":");
            int hour = Integer.parseInt(startHRandMIN[0]);
            int guiHour = gui.getHour();
            if (guiHour == 0)
                guiHour = 12;
            else if (guiHour > 12)
                guiHour -= 12;
            if (hour < guiHour || (hour == 12 && hour > guiHour))
                return "In Progress";
            else if (hour == guiHour) {
                int min = Integer.parseInt(startHRandMIN[1]);
                int guiMin = gui.getMinute();
                if (min <= guiMin)
                    return "In Progress";
                else
                    return "No";
            }
            else
                return "No";
        }
        else if (startTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_PM))
            return "In Progress";
        else
            return "No";
    }
    
    /**
     * This method gets the non-preset events from the list.
     * 
     * @param list - the list, usually of all the events in a month
     * @return a list of non-preset events
     */
    private ObservableList getNewEventsList(ObservableList list) {
        for (int i = 0; i < newEvents.size(); i++) {
            Event e = (Event) newEvents.get(i);
            if (gui.getEditedYear() == e.getYear()) {
                String[] date = e.getDate().split("/");
                if (Integer.parseInt(date[0]) == gui.getMonth(gui.getCurrentMonth())) {
                    list.add(e);
                    newEvents.remove(e);
                    eventsInUse.add(e);
                    i--;
                }
            }
        }
        return list;
    }
}