package calendar.error;

import calendar.CAL_PropertyType;
import static calendar.CAL_StartupConstants.OK_BUTTON_LABEL;
import calendar.gui.MessageDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class provides a response for every type of error we anticipate
 * inside of our program. It's convenient to do this in one place so
 * that we have a common format for dealing with problems.
 *
 * @author MAVIRI
 */
public class ErrorHandler {
    //use a singleton design pattern
    static ErrorHandler singleton;
    
    //message feedback
    MessageDialog messageDialog;
    
    //properties for feedback
    PropertiesManager props;
    
    //constructor only to be accessed in here
    private ErrorHandler() {
        //do we need to construct the singleton?
        singleton = null;
        props = PropertiesManager.getPropertiesManager();
    }
    
     /**
     * This method initializes this error handler's message dialog
     * so that it may provide feedback when errors occur.
     * 
     * @param owner - the parent window for the modal message dialog.
     */
    public void initMessageDialog(Stage owner) {
        //use this dialog to provide feedback
        messageDialog = new MessageDialog(owner, OK_BUTTON_LABEL);
    }
    
    /**
     * This accessor method gets this error handler singleton.
     * 
     * @return The singleton ErrorHandler used by the entire
     *         application for responding to error conditions.
     */
    public static ErrorHandler getErrorHandler() {
        //initialize the singleton only once
        if (singleton == null)
            singleton = new ErrorHandler();
        
        //and return
        return singleton;
    }
    
    /**
     * Handles the event that we had an error creating a new calendar.
     */
    public void handleNewCalendarError() {
        messageDialog.show(props.getProperty(CAL_PropertyType.NEW_CALENDAR_ERROR_MESSAGE));
    }
    
    /**
     * Handles the event that we had an error saving the calendar.
     */
    public void handleSaveCalendarError() {
        messageDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_SAVED_ERROR_MESSAGE));
    }
    
    /**
     * Handles the event that we had an error loading the calendar.
     */
    public void handleLoadCalendarError() {
        messageDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_LOAD_ERROR_MESSAGE));
    }
    
    /**
     * Handles the event that we had an error exiting the program.
     */
    public void handleExitError() {
        messageDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_EXIT_ERROR_MESSAGE));
    }
    
    /**
     * Handles the event that the properties.xml file cannot be loaded.
     */
    public void handlePropertiesFileError() {
        messageDialog.show(props.getProperty(CAL_PropertyType.CALENDAR_PROPERTIES_FILE_ERROR_MESSAGE));
    }
}