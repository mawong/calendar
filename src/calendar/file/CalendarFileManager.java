package calendar.file;

import calendar.gui.CAL_GUI;
import java.io.IOException;

/**
 * This interface allows us to save and load the calendar stuff when prompted.
 *
 * @author MAVIRI
 */
public interface CalendarFileManager {
    public void                 saveCalendar(CAL_GUI guiToSave) throws IOException;
    public void                 loadCalendar(CAL_GUI guiToLoad, String calPath) throws IOException;
}