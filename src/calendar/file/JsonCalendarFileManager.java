package calendar.file;

import static calendar.CAL_StartupConstants.PATH_CALENDAR;
import calendar.data.Event;
import calendar.data.EventValues;
import calendar.gui.CAL_GUI;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;

/**
 * This class uses the JSON file format to implement the necessary functions to save and load
 * events for the calendar.
 *
 * @author MAVIRI
 */
public class JsonCalendarFileManager implements CalendarFileManager {
    //reading and writing constants
    String JSON_EVENTS = "events";
    String JSON_ACTIVITY = "activity";
    String JSON_START_TIME = "start time";
    String JSON_END_TIME = "end time";
    String JSON_DATE = "date";
    String JSON_YEAR = "year";
    String JSON_COMPLETED = "completed";
    String JSON_IS_PRESET = "isPreset";
    String JSON_IS_HOLIDAY = "isHoliday";
    String JSON_CAL_NAME = "calName";
    String JSON_EXT = ".json";
    String SLASH = "/";
    
    /**
     * This method saves the calendar as a JSON file where we can view it later.
     * 
     * @param guiToSave - the GUI application
     * @throws IOException - thrown if there is a problem with anything I/O-related
     */
    @Override
    public void saveCalendar(CAL_GUI guiToSave) throws IOException {
        //build the file path
        String calListing = "" + guiToSave.getCalendarName();
        String jsonFilePath = PATH_CALENDAR + SLASH + calListing + JSON_EXT;
        
        //init the writer
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        //make a json array for the events
        JsonArray eventsJsonArray = makeEventsJsonArray(guiToSave.getHashController().getHashValues());
        
        //now build the save file using the name and the new events
        JsonObject calJsonObject = Json.createObjectBuilder()
                                    .add(JSON_CAL_NAME, guiToSave.getCalendarName())
                                    .add(JSON_EVENTS, eventsJsonArray)
                .build();
        
        //and save everything at once
        jsonWriter.writeObject(calJsonObject);
    }

    /**
     * This method loads a calendar from a JSON save file.
     * 
     * @param guiToLoad - the GUI application
     * @param calPath - the path of the JSON file to load
     * @throws IOException - thrown if there is a problem with anything I/O-related
     */
    @Override
    public void loadCalendar(CAL_GUI guiToLoad, String calPath) throws IOException {
        //loads the json file with all the data
        JsonObject json = loadJSONFile(calPath);
        
        //now load the calendar
        guiToLoad.setCalendarName(json.getString(JSON_CAL_NAME));
        
        //get the events
        guiToLoad.getHashController().getHashValues().clearAllLists();
        guiToLoad.getHashController().clearNonPresetData();
        JsonArray eventsJsonArray = json.getJsonArray(JSON_EVENTS);
        ObservableList events = createEventList(eventsJsonArray);
        guiToLoad.getHashController().getHashValues().setNewEventsLists(events);
        
        //set the month and year to today, and update the GUI
        LocalDate today = LocalDate.now();
        guiToLoad.setEditedMonth(today.getMonth().name());
        guiToLoad.setEditedTotDaysPerMonth(today.lengthOfMonth());
        guiToLoad.setEditedYear(today.getYear());
        guiToLoad.setIntEditedMonth(today.getMonthValue());
        guiToLoad.removeCurrentDayButton();
        guiToLoad.updateMonthLabel();
        guiToLoad.disableAllButtons();
        guiToLoad.calendarTableEvents();
        guiToLoad.setTheTables();
    }
    
    /**
     * This method creates a JSON object for the event.
     * 
     * @param e - the event being turned into a JSON object
     * @return the JSON object of the event
     */
    private JsonObject makeEventJsonObject(Event e) {
        JsonObject jo = Json.createObjectBuilder().add(JSON_ACTIVITY, e.getActivity())
                                                    .add(JSON_START_TIME, e.getStartTime())
                                                    .add(JSON_END_TIME, e.getEndTime())
                                                    .add(JSON_DATE, e.getDate())
                                                    .add(JSON_YEAR, e.getYear())
                                                    .add(JSON_COMPLETED, e.getCompleted())
                                                    .add(JSON_IS_PRESET, e.getIsPreset())
                                                    .add(JSON_IS_HOLIDAY, e.getIsHoliday())
                                                    .build();
        return jo;
    }
    
    /**
     * This method makes a JSON array of the list of non-preset events from the GUI.
     * 
     * @param eventValues - the class from where we retrieve the events list
     * @return the JSON array that was created
     */
    public JsonArray makeEventsJsonArray(EventValues eventValues) {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        ObservableList events = eventValues.getAllNewEvents();
        for (Object obj : events) {
            Event e = (Event) obj;
            jab.add(makeEventJsonObject(e));
        }
        JsonArray jA = jab.build();
        return jA;
    }
    
    /**
     * This method loads a JSON file and returns it.
     * 
     * @param jsonFilePath - the path of the JSON file
     * @return the JSON object loaded from the file
     * @throws IOException - thrown if there is a problem with anything I/O-related
     */
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    /**
     * This method creates the events list from a JSON array.
     * 
     * @param eventArray - the JSON array to be converted into an ObservableList
     * @return the list to be placed in the GUI
     */
    private ObservableList createEventList(JsonArray eventArray) {
        ObservableList list = FXCollections.observableArrayList();
        for (int i = 0; i < eventArray.size(); i++) {
            JsonObject jso = eventArray.getJsonObject(i);
            Event e = new Event();
            e.setActivity(jso.getString(JSON_ACTIVITY));
            e.setCompleted(jso.getString(JSON_COMPLETED));
            e.setDate(jso.getString(JSON_DATE));
            e.setEndTime(jso.getString(JSON_END_TIME));
            e.setIsHoliday(jso.getBoolean(JSON_IS_HOLIDAY));
            e.setIsPreset(jso.getBoolean(JSON_IS_PRESET));
            e.setStartTime(jso.getString(JSON_START_TIME));
            e.setYear(jso.getInt(JSON_YEAR));
            list.add(e);
        }
        return list;
    }
}