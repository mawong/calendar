package calendar.gui;

import calendar.CAL_PropertyType;
import calendar.data.Event;
import static calendar.gui.CAL_GUI.CLASS_HEADING_LABEL;
import static calendar.gui.CAL_GUI.CLASS_SUBHEADING_LABEL;
import static calendar.gui.CAL_GUI.PRIMARY_STYLE_SHEET;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This represents the dialog box after the user clicks "Add", "Edit", or "Remove"
 * from the DayEventsDialog box. It handles everything that has to do with adding,
 * editing, and removing events. Note: users are allowed to ONLY change events they
 * created; NONE OF THE PRESET EVENTS WILL BE OVERWRITTEN
 *
 * @author MAVIRI
 */
public class AddEditRemoveEventDialog extends Stage {
    //GUI
    CAL_GUI gui;
    PropertiesManager props;
    
    //data of an event we're going to make
    Event event;
    
    //components for the UI
    GridPane gridPane;
    Scene dialogScene;
    Label dateLabel;
    Label timeStartLabel, timeStartHourLabel, timeStartMinuteLabel, timeStartPeriodLabel;
    Label timeEndLabel, timeEndHourLabel, timeEndMinuteLabel, timeEndPeriodLabel;
    Label activityLabel;
    TextField timeStartHoursTextField, timeStartMinutesTextField;
    ChoiceBox timeStartPeriodChoiceBox;
    TextField timeEndHoursTextField, timeEndMinutesTextField;
    ChoiceBox timeEndPeriodChoiceBox;
    TextField activityTextField;
    Button completeButton, cancelButton;
    
    //keep track of the user pressed button, and the other text fields and choice boxes
    String selection;
    String startHourTF, startMinuteTF, startPeriodCB;
    String endHourTF, endMinuteTF, endPeriodCB;
    String activityTF;
    
    //constants
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TIME_START_LABEL = "Start Time: ";
    public static final String TIME_END_LABEL = "End Time: ";
    public static final String ACTIVITY_LABEL = "Activity: ";
    public static final String HOUR_LABEL = "Hour: ";
    public static final String MINUTE_LABEL = "Minute: ";
    public static final String PERIOD_LABEL = "Period: ";
    public static final String PERIOD_AM = "AM";
    public static final String PERIOD_PM = "PM";
    
    /**
     * Constructor for the dialog box. Note: this doesn't have much to do with removing,
     * but I'll keep the remove part there anyway.
     * 
     * @param primaryStage - the stage that the user was viewing
     * @param messageDialog - the message dialog
     * @param initGUI - the GUI
     */
    public AddEditRemoveEventDialog(Stage primaryStage, MessageDialog messageDialog, CAL_GUI initGUI) {
        //make it modal (others will wait when this is displayed)
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gui = initGUI;
        
        //create the container
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        props = PropertiesManager.getPropertiesManager();
        
        //put the date on the grid
        dateLabel = new Label();
        dateLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //now the time start and end labels
        timeStartLabel = new Label(TIME_START_LABEL);
        timeStartLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        timeStartHourLabel = new Label(HOUR_LABEL);
        timeStartMinuteLabel = new Label(MINUTE_LABEL);
        timeStartPeriodLabel = new Label(PERIOD_LABEL);
        timeEndLabel = new Label(TIME_END_LABEL);
        timeEndLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        timeEndHourLabel = new Label(HOUR_LABEL);
        timeEndMinuteLabel = new Label(MINUTE_LABEL);
        timeEndPeriodLabel = new Label(PERIOD_LABEL);
        
        //and create their text fields
        timeStartHoursTextField = new TextField("");
        timeStartHoursTextField.setPrefColumnCount(2);
        timeStartMinutesTextField = new TextField("");
        timeStartMinutesTextField.setPrefColumnCount(2);
        timeEndHoursTextField = new TextField("");
        timeEndHoursTextField.setPrefColumnCount(2);
        timeEndMinutesTextField = new TextField("");
        timeEndMinutesTextField.setPrefColumnCount(2);
        
        //and add in the choice boxes
        timeStartPeriodChoiceBox = new ChoiceBox();
        timeStartPeriodChoiceBox.setMinWidth(55);
        timeEndPeriodChoiceBox = new ChoiceBox();
        timeEndPeriodChoiceBox.setMinWidth(55);
        
        //now the activity label and text field
        activityLabel = new Label(ACTIVITY_LABEL);
        activityLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        activityTextField = new TextField("");
        activityTextField.setMinWidth(75);
        
        //and now the buttons
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        //register the text fields
        registerHourTextField(timeStartHoursTextField, 0);
        registerHourTextField(timeEndHoursTextField, 1);
        registerMinuteTextField(timeStartMinutesTextField, 0);
        registerMinuteTextField(timeEndMinutesTextField, 1);
        registerActivityTextField(activityTextField);
        
        //register the choice boxes
        timeStartPeriodChoiceBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.toString() != null) {
                if (newValue.toString().equals("0"))
                    startPeriodCB = "";
                else if (newValue.toString().equals("1"))
                    startPeriodCB = PERIOD_AM;
                else
                    startPeriodCB = PERIOD_PM;
            }
        });
        
        timeEndPeriodChoiceBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.toString() != null) {
                if (newValue.toString().equals("0"))
                    endPeriodCB = "";
                else if (newValue.toString().equals("1"))
                    endPeriodCB = PERIOD_AM;
                else
                    endPeriodCB = PERIOD_PM;
            }
        });
        
        //register the buttons
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AddEditRemoveEventDialog.this.selection = sourceButton.getText();
            if (selection.equals(COMPLETE)) {
                boolean good = checkForValidity(messageDialog, props);
                if (good)
                    AddEditRemoveEventDialog.this.hide();
            }
            else
                AddEditRemoveEventDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //and finally place them on the grid pane
        gridPane.add(dateLabel, 0, 0, 5, 1);
        gridPane.add(timeStartLabel, 0, 2, 3, 1);
        gridPane.add(timeStartHourLabel, 0, 3, 1, 1);
        gridPane.add(timeStartHoursTextField, 1, 3, 1, 1);
        gridPane.add(timeStartMinuteLabel, 2, 3, 1, 1);
        gridPane.add(timeStartMinutesTextField, 3, 3, 1, 1);
        gridPane.add(timeStartPeriodLabel, 4, 3, 1, 1);
        gridPane.add(timeStartPeriodChoiceBox, 5, 3, 1, 1);
        gridPane.add(timeEndLabel, 0, 5, 3, 1);
        gridPane.add(timeEndHourLabel, 0, 6, 1, 1);
        gridPane.add(timeEndHoursTextField, 1, 6, 1, 1);
        gridPane.add(timeEndMinuteLabel, 2, 6, 1, 1);
        gridPane.add(timeEndMinutesTextField, 3, 6, 1, 1);
        gridPane.add(timeEndPeriodLabel, 4, 6, 1, 1);
        gridPane.add(timeEndPeriodChoiceBox, 5, 6, 1, 1);
        gridPane.add(activityLabel, 0, 8, 3, 1);
        gridPane.add(activityTextField, 0, 9, 6, 1);
        gridPane.add(completeButton, 0, 11, 1, 1);
        gridPane.add(cancelButton, 1, 11, 1, 1);
        
        //and put the dialog scene in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * This method gets the selection the user pressed.
     * 
     * @return the selection the user pressed
     */
    public String getSelection() {
        return selection;
    }
    
    /**
     * This method gets the event the user created/edited.
     * 
     * @return the event created or edited
     */
    public Event getEvent() {
        return event;
    }
    
    /**
     * This method returns true if complete was selected.
     * 
     * @return true if complete was selected, false otherwise
     */
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    /**
     * This method shows the add events dialog box.
     * 
     * @param month - the viewing month
     * @param day - the viewing day
     * @param year - the viewing year
     */
    public void showAddEventDialog(int month, String day, int year) {
        setTitle(props.getProperty(CAL_PropertyType.ADD_EVENT_TITLE));
        
        //create the event
        event = new Event();
        
        //get the date
        dateLabel.setText(month + "/" + day + "/" + year);
        
        //set the fields
        timeStartHoursTextField.setText("");
        startHourTF = "";
        timeStartMinutesTextField.setText("");
        startMinuteTF = "";
        timeEndHoursTextField.setText("");
        endHourTF = "";
        timeEndMinutesTextField.setText("");
        endMinuteTF = "";
        activityTextField.setText("");
        activityTF = "";
        timeStartPeriodChoiceBox.getItems().clear();
        timeStartPeriodChoiceBox.getItems().addAll("", PERIOD_AM, PERIOD_PM);
        startPeriodCB = "";
        timeEndPeriodChoiceBox.getItems().clear();
        timeEndPeriodChoiceBox.getItems().addAll("", PERIOD_AM, PERIOD_PM);
        endPeriodCB = "";
        
        //show it up
        this.showAndWait();
    }
    
    /**
     * This method shows the edit event dialog box.
     * 
     * @param month - the viewing month
     * @param day - the viewing day
     * @param year - the viewing year
     * @param eventToEdit - the event we are editing
     */
    public void showEditEventDialog(int month, String day, int year, Event eventToEdit) {
        setTitle(props.getProperty(CAL_PropertyType.EDIT_EVENT_TITLE));
        
        //create the event
        event = new Event();
        event.setStartTime(eventToEdit.getStartTime());
        event.setEndTime(eventToEdit.getEndTime());
        event.setActivity(eventToEdit.getActivity());
        
        //get the date
        dateLabel.setText(month + "/" + day + "/" + year);
        
        //set the start time and period
        String[] eventTimeAndPeriod = eventToEdit.getStartTime().split(" ");
        String[] eventHRandMIN = eventTimeAndPeriod[0].split(":");
        timeStartHoursTextField.setText(eventHRandMIN[0]);
        startHourTF = eventHRandMIN[0];
        timeStartMinutesTextField.setText(eventHRandMIN[1]);
        startMinuteTF = eventHRandMIN[1];
        timeStartPeriodChoiceBox.getItems().clear();
        timeStartPeriodChoiceBox.getItems().addAll("", PERIOD_AM, PERIOD_PM);
        if (eventTimeAndPeriod[1].equals(PERIOD_AM)) {
            timeStartPeriodChoiceBox.setValue(PERIOD_AM);
            startPeriodCB = PERIOD_AM;
        }
        else {
            timeStartPeriodChoiceBox.setValue(PERIOD_PM);
            startPeriodCB = PERIOD_PM;
        }
        
        //set the end time and period
        eventTimeAndPeriod = eventToEdit.getEndTime().split(" ");
        eventHRandMIN = eventTimeAndPeriod[0].split(":");
        timeEndHoursTextField.setText(eventHRandMIN[0]);
        endHourTF = eventHRandMIN[0];
        timeEndMinutesTextField.setText(eventHRandMIN[1]);
        endMinuteTF = eventHRandMIN[1];
        timeEndPeriodChoiceBox.getItems().clear();
        timeEndPeriodChoiceBox.getItems().addAll("", PERIOD_AM, PERIOD_PM);
        if (eventTimeAndPeriod[1].equals(PERIOD_AM)) {
            timeEndPeriodChoiceBox.setValue(PERIOD_AM);
            endPeriodCB = PERIOD_AM;
        }
        else {
            timeEndPeriodChoiceBox.setValue(PERIOD_PM);
            endPeriodCB = PERIOD_PM;
        }
        
        //set the activity
        activityTextField.setText(eventToEdit.getActivity());
        activityTF = eventToEdit.getActivity();
        
        //show it up
        this.showAndWait();
    }
    
    /**
     * This method registers the hour text fields, sOe = numbers (start or end)
     * 
     * @param textField - the textfield we are registering
     * @param sOe - a number: 0 = start, 1 = end
     */
    private void registerHourTextField(TextField textField, int sOe) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (sOe == 0)
                startHourTF = textField.textProperty().getValue();
            else
                endHourTF = textField.textProperty().getValue();
        });
    }
    
    /**
     * This method registers the minute text fields, sOe = numbers (start or end)
     * 
     * @param textField - the textfield we are registering
     * @param sOe - a number: 0 = start, 1 = end
     */
    private void registerMinuteTextField(TextField textField, int sOe) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (sOe == 0)
                startMinuteTF = textField.textProperty().getValue();
            else
                endMinuteTF = textField.textProperty().getValue();
        });
    }
    
    /**
     * This method registers the activity text field.
     * 
     * @param textField - the activity textfield
     */
    private void registerActivityTextField(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            activityTF = textField.textProperty().getValue();
        });
    }
    
    /**
     * This method checks the validity of the events. This is done once the user presses "COMPLETE".
     * 
     * @param messageDialog - the message dialog
     * @param props - the properties manager
     * @return true if the event is valid (save it), false if it is not (disregard it for now)
     */
    private boolean checkForValidity(MessageDialog messageDialog, PropertiesManager props) {
        //test the start hour
        if (startHourTF.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.TEXT_FIELD_EMPTY_MESSAGE));
            return false;
        }
        for (int i = 0; i < startHourTF.length(); i++) {
            char ch = startHourTF.charAt(i);
            if (!Character.isDigit(ch)) {
                messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_DIGIT_MESSAGE));
                return false;
            }
        }
        int tempInt = Integer.parseInt(startHourTF);
        if (tempInt < 1 || tempInt > 12) {
            messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_NUMBERS_MESSAGE));
            return false;
        }

        //check the start minute
        if (startMinuteTF.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.TEXT_FIELD_EMPTY_MESSAGE));
            return false;
        }
        for (int i = 0; i < startMinuteTF.length(); i++) {
            char ch = startMinuteTF.charAt(i);
            if (!Character.isDigit(ch)) {
                messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_DIGIT_MESSAGE));
                return false;
            }
        }
        tempInt = Integer.parseInt(startMinuteTF);
        if (tempInt < 0 || tempInt > 59) {
            messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_NUMBERS_MESSAGE));
            return false;
        }
        if (tempInt < 10 && startMinuteTF.length() == 1) {
            startMinuteTF = "0" + startMinuteTF;
        }
        
        //check the start period
        if (startPeriodCB.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.NO_PERIOD_PICKED_MESSAGE));
            return false;
        }
        event.setStartTime(startHourTF + ":" + startMinuteTF + " " + startPeriodCB);
        
        //now, check the end times; test the end hour
        if (endHourTF.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.TEXT_FIELD_EMPTY_MESSAGE));
            return false;
        }
        for (int i = 0; i < endHourTF.length(); i++) {
            char ch = endHourTF.charAt(i);
            if (!Character.isDigit(ch)) {
                messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_DIGIT_MESSAGE));
                return false;
            }
        }
        tempInt = Integer.parseInt(endHourTF);
        if (tempInt < 1 || tempInt > 12) {
            messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_NUMBERS_MESSAGE));
            return false;
        }
    
        //check the end minute
        if (endMinuteTF.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.TEXT_FIELD_EMPTY_MESSAGE));
            return false;
        }
        for (int i = 0; i < endMinuteTF.length(); i++) {
            char ch = endMinuteTF.charAt(i);
            if (!Character.isDigit(ch)) {
                messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_DIGIT_MESSAGE));
                return false;
            }
        }
        tempInt = Integer.parseInt(endMinuteTF);
        if (tempInt < 0 || tempInt > 59) {
            messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_TIME_NUMBERS_MESSAGE));
            return false;
        }
        if (tempInt < 10 && endMinuteTF.length() == 1)
            endMinuteTF = "0" + endMinuteTF;
        
        //check the end period
        if (endPeriodCB.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.NO_PERIOD_PICKED_MESSAGE));
            return false;
        }
        event.setEndTime(endHourTF + ":" + endMinuteTF + " " + endPeriodCB);
        
        if (endPeriodCB.equals(PERIOD_AM) && startPeriodCB.equals(PERIOD_PM)) {
            messageDialog.show(props.getProperty(CAL_PropertyType.END_TIME_BEFORE_START_TIME_MESSAGE));
            return false;
        }
        else if ((endPeriodCB.equals(PERIOD_AM) && startPeriodCB.equals(PERIOD_AM)) || endPeriodCB.equals(PERIOD_PM) && startPeriodCB.equals(PERIOD_PM)) {
            if (Integer.parseInt(endHourTF) < Integer.parseInt(startHourTF) && Integer.parseInt(startHourTF) != 12) {
                messageDialog.show(props.getProperty(CAL_PropertyType.END_TIME_BEFORE_START_TIME_MESSAGE));
                return false;
            }
            else if (((startPeriodCB.equals(PERIOD_AM) && Integer.parseInt(startHourTF) != 12) && (endPeriodCB.equals(PERIOD_AM) && Integer.parseInt(endHourTF) == 12))
                    || ((startPeriodCB.equals(PERIOD_PM) && Integer.parseInt(startHourTF) != 12) && (endPeriodCB.equals(PERIOD_PM) && Integer.parseInt(endHourTF) == 12))) {
                messageDialog.show(props.getProperty(CAL_PropertyType.END_TIME_BEFORE_START_TIME_MESSAGE));
                return false;
            }
            else if (Integer.parseInt(endHourTF) == Integer.parseInt(startHourTF)) {
                if (Integer.parseInt(endMinuteTF) < Integer.parseInt(startMinuteTF)) {
                    messageDialog.show(props.getProperty(CAL_PropertyType.END_TIME_BEFORE_START_TIME_MESSAGE));
                return false;
                }
            }
        }
        
        //set the dates and the year
        String[] date = dateLabel.getText().split("/");
        event.setDate(date[0] + "/" + date[1]);
        event.setYear(Integer.parseInt(date[2]));
        
        //set the activity
        if (activityTF.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.TEXT_FIELD_EMPTY_MESSAGE));
            return false;
        }
        event.setActivity(activityTF);
        event.setCompleted(setEventCompleted(event.getStartTime(), event.getEndTime()));
        return true;
    }
    
    /**
     * This method sees if the event is already completed (Yes, No, or In Progress).
     * 
     * @param startTime - the event's start time
     * @param endTime - the event's end time
     * @return the string, "Yes", "No", or "In Progress", for the event's value: completed
     */
    private String setEventCompleted(String startTime, String endTime) {
        if (event.getYear() > gui.getCurrentYear())
            return "No";
        else if (event.getYear() < gui.getCurrentYear())
            return "Yes";
        String[] temp = event.getDate().split("/");
        if (Integer.parseInt(temp[0]) > gui.getIntCurrentMonth())
            return "No";
        else if (Integer.parseInt(temp[0]) < gui.getIntCurrentMonth())
            return "Yes";
        if (Integer.parseInt(temp[1]) > gui.getCurrentDay())
            return "No";
        else if (Integer.parseInt(temp[1]) < gui.getCurrentDay())
            return "Yes";
        String[] endTimeAndPeriod = endTime.split(" ");
        if ((endTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_AM)) || (endTimeAndPeriod[1].equals(PERIOD_PM) && gui.getPeriod().equals(PERIOD_PM))) {
            String[] endHRandMIN = endTimeAndPeriod[0].split(":");
            int hour = Integer.parseInt(endHRandMIN[0]);
            int guiHour = gui.getHour();
            if (guiHour == 0)
                guiHour = 12;
            else if (guiHour > 12)
                guiHour -= 12;
            if (hour < guiHour || (hour == 12 && hour > guiHour))
                return "Yes";
            else if (hour == guiHour) {
                int min = Integer.parseInt(endHRandMIN[1]);
                int guiMin = gui.getMinute();
                if (min <= guiMin)
                    return "Yes";
                else {
                    return getStartTime(startTime);
                }
            }
            else
                return getStartTime(startTime);
        }
        else if ((endTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_PM)))
            return "Yes";
        else
            return getStartTime(startTime);
    }
    
    /**
     * This method helps set the completed event (Yes, No, or In Progress). The previous event
     * checked the end time; this event checks the start time.
     * 
     * @param startTime - the event's start time
     * @return the string, "Yes", "No", or "In Progress", for the event's value: completed
     */
    private String getStartTime(String startTime) {
        String[] startTimeAndPeriod = startTime.split(" ");
        if ((startTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_AM)) || (startTimeAndPeriod[1].equals(PERIOD_PM) && gui.getPeriod().equals(PERIOD_PM))) {
            String[] startHRandMIN = startTimeAndPeriod[0].split(":");
            int hour = Integer.parseInt(startHRandMIN[0]);
            int guiHour = gui.getHour();
            if (guiHour == 0)
                guiHour = 12;
            else if (guiHour > 12)
                guiHour -= 12;
            if (hour < guiHour || (hour == 12 && hour > guiHour))
                return "In Progress";
            else if (hour == guiHour) {
                int min = Integer.parseInt(startHRandMIN[1]);
                int guiMin = gui.getMinute();
                if (min <= guiMin)
                    return "In Progress";
                else
                    return "No";
            }
            else
                return "No";
        }
        else if (startTimeAndPeriod[1].equals(PERIOD_AM) && gui.getPeriod().equals(PERIOD_PM))
            return "In Progress";
        else
            return "No";
    }
}