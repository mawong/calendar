package calendar.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class works with putting pop-ups on the screen when an event is just about
 * to start. When it starts, the pop-up dialog alert is presented to the screen, and
 * the user must click the OK button so the program knows they have seen it.
 *
 * @author MAVIRI
 */
public class Alerts {
    //constructor
    public Alerts() {}
    
    /**
     * This method shows the dialog when an event begins. It presents the dialog on 
     * the screen and waits for the user to click OK.
     * 
     * @param activity - the name of the activity about to begin.
     */
    public void EventStartAlert(String activity) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("New Event Started");
        alert.setHeaderText("A new event has begun!");
        alert.setContentText("The event \"" + activity + "\" has started.\n\nClick OK to dismiss.");
        
        alert.showAndWait();
    }
}
