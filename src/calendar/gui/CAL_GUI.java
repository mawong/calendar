package calendar.gui;

import calendar.CAL_PropertyType;
import static calendar.CAL_StartupConstants.*;
import calendar.controller.CalendarController;
import calendar.controller.FileController;
import calendar.controller.EventController;
import calendar.data.Event;
import calendar.file.CalendarFileManager;
import calendar.init.InitializeGui;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This is the GUI for the calendar. It manages all the UI components for editing 
 * the calendar and adding items to days and/or months.
 * 
 * @author MAVIRI
 */
public class CAL_GUI {
    //constants
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "cal_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_MONTH_LABEL = "month_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_TODAY_BUTTON = "today_button";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TABLE = "table";
    static final String CLASS_COLUMNS = "column";
    static final String CLASS_EVENTS_HOLIDAY = "date_holiday";
    static final String CLASS_EVENTS_REGULAR = "date_event";
    
    //our stage and scene
    Stage primaryStage;
    Scene primaryScene;
    
    //controllers
    FileController fileController;
    CalendarController calController;
    EventController eventController;
    CalendarFileManager calendarIO;
    
    //our main border pane
    BorderPane calPane;
    
    //gui initializer
    InitializeGui initGui;
    
    //controls - calendar pane, headings
    GridPane calendarGridPane;
    BorderPane toolbarPane;
    VBox leftVBox, rightVBox;
    HBox fileButtonsHBox, calendarNameHBox;
    Label monthLabel, sundayLabel, mondayLabel, tuesdayLabel, wednesdayLabel, thursdayLabel, fridayLabel, saturdayLabel;
    
    //file buttons
    Button newButton, loadButton, saveButton, exitButton;
    
    //calendar name stuff
    Label calendarNameLabel;
    TextField calendarNameTF;
    String calendarName;
    
    //button weeks
    Button button00, button01, button02, button03, button04, button05, button06;
    Button button10, button11, button12, button13, button14, button15, button16;
    Button button20, button21, button22, button23, button24, button25, button26;
    Button button30, button31, button32, button33, button34, button35, button36;
    Button button40, button41, button42, button43, button44, button45, button46;
    Button button50, button51, button52, button53, button54, button55, button56;
    
    //button that tells us the current day
    Button currentDayButton;
    
    //buttons to move to a different month or different year
    Button prevMonthButton, nextMonthButton, changeMonthButton, changeYearButton;
    
    //dates
    String currentMonth;
    int intCurrentMonth, currentDay, currentYear, totDaysPerMonth, monthStartDOW;
    
    String editedMonth;
    int intEditedMonth, editedYear, editedTotDaysPerMonth, hour, minute;
    String period;
    
    //label to show the time
    Label timeLabel;
    
    //label to show the events
    Label monthEventsLabel, dayEventsLabel;
    
    //table for the events
    TableView monthEventsTable, dayEventsTable;
    
    //table columns for the table
    TableColumn monthDateColumn, monthActivityColumn, monthCompletedColumn;
    TableColumn dayStartTimeColumn, dayEndTimeColumn, dayActivityColumn, dayCompletedColumn;
    
    //boolean for the thread tast
    boolean running;
    
    //list to get the buttons
    List<Button> buttonList;
    
    //constants
    final String DOW_SUNDAY = "Sun";
    final String DOW_MONDAY = "Mon";
    final String DOW_TUESDAY = "Tue";
    final String DOW_WEDNESDAY = "Wed";
    final String DOW_THURSDAY = "Thu";
    final String DOW_FRIDAY = "Fri";
    final String DOW_SATURDAY = "Sat";
    static final String CALENDAR_NAME_LABEL = "Name: ";
    static final String M_JANUARY = "JANUARY";
    static final String M_FEBRUARY = "FEBRUARY";
    static final String M_MARCH = "MARCH";
    static final String M_APRIL = "APRIL";
    static final String M_MAY = "MAY";
    static final String M_JUNE = "JUNE";
    static final String M_JULY = "JULY";
    static final String M_AUGUST = "AUGUST";
    static final String M_SEPTEMBER = "SEPTEMBER";
    static final String M_OCTOBER = "OCTOBER";
    static final String M_NOVEMBER = "NOVEMBER";
    static final String M_DECEMBER = "DECEMBER";
    static final String COL_DATE = "Date";
    static final String COL_ACTIVITY = "Activity";
    static final String COL_COMPLETED = "Completed";
    static final String COL_START_TIME = "Start";
    static final String COL_END_TIME = "End";
    
    //dialogs
    MessageDialog messageDialog;
    YesNoCancelDialog yncDialog;
    
    /**
     * Constructor for making the GUI
     * 
     * @param initStage the stage
     */
    public CAL_GUI(Stage initStage) {
        primaryStage = initStage;
    }
    
    /**
     * This method gets the stage from which the window is set (what the user sees right now).
     * 
     * @return the stage
     */
    public Stage getWindow() {
        return primaryStage;
    }
      
    /**
     * This method gets the message dialog.
     * 
     * @return the message dialog
     */
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    /**
     * This method sets the file manager so we can do all the I/O for this application.
     * 
     * @param fileManager - the pre-made file manager from Calendar.java
     */
    public void setCalendarFileManager(CalendarFileManager fileManager) {
        calendarIO = fileManager;
    }
    
    /**
     * This method gets the file controller to do file options (new, save, load, exit)
     * 
     * @return the file controller
     */
    public FileController getFileController() {
        return fileController;
    }
    
    /**
     * This method gets the event controller -- the controller where the data is set.
     * 
     * @return the event controller
     */
    public EventController getHashController() {
        return eventController;
    }
    
    /**
     * This method gets the calendar name (string) that is from the textfield.
     * 
     * @return the calendar name
     */
    public String getCalendarName() {
        return calendarName;
    }
    
    /**
     * This method sets the name of the calendar (string) and sets the textfield's text.
     * 
     * @param initCalName - the name of the calendar
     */
    public void setCalendarName(String initCalName) {
        calendarName = initCalName;
        calendarNameTF.setText(initCalName);
    }
    
    /**
     * This method gets the month we are seeing in the calendar display right now.
     * 
     * @return the month we are viewing right now (JANUARY, etc.)
     */
    public String getEditedMonth() {
        return editedMonth;
    }
    
    /**
     * This method sets the month we want to see in the calendar display right now.
     * 
     * @param newEditedMonth - the month we want to view right now
     */
    public void setEditedMonth(String newEditedMonth) {
        editedMonth = newEditedMonth;
    }
    
    /**
     * This method gets the integer value of the month we are seeing in the calendar display.
     * 
     * @return the integer month we are viewing right now (1 - 12)
     */
    public int getIntEditedMonth() {
        return intEditedMonth;
    }
    
    /**
     * This method sets the integer value of the month we want to see in the calendar display.
     * 
     * @param newEditedMonth - the integer month we want to view right now
     */
    public void setIntEditedMonth(int newEditedMonth) {
        intEditedMonth = newEditedMonth;
    }
    
    /**
     * This method gets the year we are seeing in the calendar display right now.
     * 
     * @return the year we are viewing right now
     */
    public int getEditedYear() {
        return editedYear;
    }
    
    /**
     * This method sets the year we want to see in the calendar display right now.
     * 
     * @param newEditedYear - the year we want to view right now
     */
    public void setEditedYear(int newEditedYear) {
        editedYear = newEditedYear;
    }
    
    /**
     * This method sets the total days of the month we are seeing in the calendar display.
     * 
     * @param newEditedTotDays - the total days of the month we are viewing right now
     */
    public void setEditedTotDaysPerMonth(int newEditedTotDays) {
        editedTotDaysPerMonth = newEditedTotDays;
    }
    
    /**
     * This method gets the current month.
     * 
     * @return the current month
     */
    public String getCurrentMonth() {
        return currentMonth;
    }
    
    /**
     * This method gets the integer value of the current month
     * 
     * @return the integer value of the current month
     */
    public int getIntCurrentMonth() {
        return intCurrentMonth;
    }
    
    /**
     * This method gets the total days of the current month.
     * 
     * @return the total days of the current month
     */
    public int getCurrentTotDaysPerMonth() {
        return totDaysPerMonth;
    }
    
    /**
     * This method gets the current day
     * 
     * @return the current day
     */
    public int getCurrentDay() {
        return currentDay;
    }
    
    /**
     * This method gets the current year.
     * 
     * @return the current year
     */
    public int getCurrentYear() {
        return currentYear;
    }
    
    /**
     * This method gets the current period (AM/PM).
     * 
     * @return AM or PM, depending on the current time
     */
    public String getPeriod() {
        return period;
    }
    
    /**
     * This method gets the current hour.
     * 
     * @return the current hour
     */
    public int getHour() {
        return hour;
    }
    
    /**
     * This method gets the current minute
     * 
     * @return the current minute
     */
    public int getMinute() {
        return minute;
    }
    
    /**
     * This method initializes the GUI
     * 
     * @param windowTitle the title of the window
     */
    public void initGUI(String windowTitle) {
        //set up initializers
        initGui = new InitializeGui();
        
        //init everything
        initDialogs();
        initWorkspace();
        initEventHandlers();
        updateToolbarControls(true);
        initWindow(windowTitle);
    }
    
    /**
     * This method creates and sets up all the dialogs we will be using.
     */
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, OK_BUTTON_LABEL);
        yncDialog = new YesNoCancelDialog(primaryStage);
    }
    
    /**
     * This method creates and sets up all the controls of our workspace.
     */
    private void initWorkspace() {
        initTopPane();
        initLeftPane();
        initRightPane();
        initCenterPane();
        
        calPane = new BorderPane();
        calPane.setTop(toolbarPane);
        calPane.setCenter(calendarGridPane);
        calPane.setLeft(leftVBox);
        calPane.setRight(rightVBox);
        calPane.getStyleClass().add(CLASS_BORDERED_PANE);
    }
    /**
     * This method initializes the window (our screen) of which the application will load onto.
     * 
     * @param title - the title of the application
     */
    private void initWindow(String title) {
        //set the title
        primaryStage.setTitle(title);
        
        //set the screen
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        //set the size of the window
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        
        //set the scene
        primaryScene = new Scene(calPane);
        
        //tie scene to window and open it up
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
    /**
     * This method creates the top pane, with the file toolbar and the name of the calendar.
     */
    private void initTopPane() {
        //first, we place the important dates in the lists, so initialize this early
        eventController = new EventController(this);
        
        //create the top pane
        toolbarPane = new BorderPane();
        toolbarPane.setPadding(new Insets(10, 10, 10, 10));
        fileButtonsHBox = new HBox();
        calendarNameHBox = new HBox();
        
        //contents of the top pane
        //calendar name
        calendarNameLabel = new Label(CALENDAR_NAME_LABEL);
        calendarNameLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        calendarNameTF = new TextField();
        calendarNameTF.setMinWidth(250);
        calendarNameTF.textProperty().addListener((observable, oldValue, newValue) -> {
            calendarName = calendarNameTF.textProperty().getValue();
        });
        calendarNameHBox.getChildren().add(calendarNameLabel);
        calendarNameHBox.getChildren().add(calendarNameTF);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //create a new calendar
        newButton = new Button(props.getProperty(CAL_PropertyType.NEW_TEXT));
        newButton.setTooltip(new Tooltip(props.getProperty(CAL_PropertyType.NEW_TOOLTIP)));
        
        //load the calendar
        loadButton = new Button(props.getProperty(CAL_PropertyType.LOAD_TEXT));
        loadButton.setTooltip(new Tooltip(props.getProperty(CAL_PropertyType.LOAD_TOOLTIP)));
        
        //save the calendar
        saveButton = new Button(props.getProperty(CAL_PropertyType.SAVE_TEXT));
        saveButton.setTooltip(new Tooltip(props.getProperty(CAL_PropertyType.SAVE_TOOLTIP)));
        
        //exit the program
        exitButton = new Button(props.getProperty(CAL_PropertyType.EXIT_TEXT));
        exitButton.setTooltip(new Tooltip(props.getProperty(CAL_PropertyType.EXIT_TOOLTIP)));
        
        fileButtonsHBox.getChildren().add(newButton);
        fileButtonsHBox.getChildren().add(loadButton);
        fileButtonsHBox.getChildren().add(saveButton);
        fileButtonsHBox.getChildren().add(exitButton);
        
        toolbarPane.setLeft(fileButtonsHBox);
        toolbarPane.setRight(calendarNameHBox);
    }
    
    /**
     * This method creates the left pane, with the time and today's events.
     */
    private void initLeftPane() {
        //the VBox where everythng will be placed
        leftVBox = new VBox();
        leftVBox.setMaxWidth(370);
        leftVBox.setPadding(new Insets(20, 0, 0, 0));
        leftVBox.setSpacing(30);
        
        //create the label and get the time rolling
        timeLabel = new Label();
        timeLabel.setMinWidth(370);
        timeLabel.setStyle("-fx-text-alignment: CENTER; -fx-alignment: CENTER");
        timeLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        leftVBox.getChildren().add(timeLabel);
        handleTimeThread();
        
        //create the day events label
        dayEventsLabel = initGui.initLabel(CAL_PropertyType.DAY_EVENTS_LABEL, CLASS_SUBHEADING_LABEL);
        dayEventsLabel.setMinWidth(370);
        dayEventsLabel.setStyle("-fx-alignment: CENTER");
        leftVBox.getChildren().add(dayEventsLabel);
        
        //now the table and the columns
        dayEventsTable = new TableView();
        dayEventsTable.getStyleClass().add(CLASS_TABLE);
        
        dayStartTimeColumn = new TableColumn(COL_START_TIME);
        dayStartTimeColumn.setMaxWidth(65);
        dayStartTimeColumn.setStyle("-fx-alignment: CENTER");
        dayStartTimeColumn.getStyleClass().add(CLASS_COLUMNS);
        dayEndTimeColumn = new TableColumn(COL_END_TIME);
        dayEndTimeColumn.setMaxWidth(70);
        dayEndTimeColumn.setStyle("-fx-alignment: CENTER");
        dayEndTimeColumn.getStyleClass().add(CLASS_COLUMNS);
        dayActivityColumn = new TableColumn(COL_ACTIVITY);
        dayActivityColumn.setMinWidth(149);
        dayActivityColumn.getStyleClass().add(CLASS_COLUMNS);
        dayCompletedColumn = new TableColumn(COL_COMPLETED);
        dayCompletedColumn.setMinWidth(80);
        dayCompletedColumn.setStyle("-fx-alignment: CENTER");
        dayCompletedColumn.getStyleClass().add(CLASS_COLUMNS);
        
        dayStartTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("starttime"));
        dayEndTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endtime"));
        dayActivityColumn.setCellValueFactory(new PropertyValueFactory<String, String>("activity"));
        dayCompletedColumn.setCellValueFactory(new PropertyValueFactory<String, String>("completed"));
        dayEventsTable.getColumns().add(dayStartTimeColumn);
        dayEventsTable.getColumns().add(dayEndTimeColumn);
        dayEventsTable.getColumns().add(dayActivityColumn);
        dayEventsTable.getColumns().add(dayCompletedColumn);
        
        leftVBox.getChildren().add(dayEventsTable);
    }
    
    /**
     * This method creates the right pane, with the month's events.
     */
    private void initRightPane() {
        //create the right VBox
        rightVBox = new VBox();
        rightVBox.setMaxWidth(370);
        rightVBox.setPadding(new Insets(40, 0, 0, 0));
        rightVBox.setSpacing(30);
        
        //getting the month events label
        monthEventsLabel = initGui.initLabel(CAL_PropertyType.MONTH_EVENTS_LABEL, CLASS_SUBHEADING_LABEL);
        monthEventsLabel.setMinWidth(370);
        monthEventsLabel.setStyle("-fx-alignment: CENTER");
        rightVBox.getChildren().add(monthEventsLabel);
        
        //now the table and columns
        monthEventsTable = new TableView();
        monthEventsTable.getStyleClass().add(CLASS_TABLE);
        
        monthDateColumn = new TableColumn(COL_DATE);
        monthDateColumn.setMinWidth(70);
        monthDateColumn.setStyle("-fx-alignment: CENTER");
        monthDateColumn.getStyleClass().add(CLASS_COLUMNS);
        monthActivityColumn = new TableColumn(COL_ACTIVITY);
        monthActivityColumn.setMinWidth(214);
        monthActivityColumn.getStyleClass().add(CLASS_COLUMNS);
        monthCompletedColumn = new TableColumn(COL_COMPLETED);
        monthCompletedColumn.setMinWidth(80);
        monthCompletedColumn.setStyle("-fx-alignment: CENTER");
        monthCompletedColumn.getStyleClass().add(CLASS_COLUMNS);
        
        monthDateColumn.setCellValueFactory(new PropertyValueFactory<String, String>("date"));
        monthActivityColumn.setCellValueFactory(new PropertyValueFactory<String, String>("activity"));
        monthCompletedColumn.setCellValueFactory(new PropertyValueFactory<String, String>("completed"));
        monthEventsTable.getColumns().add(monthDateColumn);
        monthEventsTable.getColumns().add(monthActivityColumn);
        monthEventsTable.getColumns().add(monthCompletedColumn);
        
        rightVBox.getChildren().add(monthEventsTable);
    }
    
    /**
     * This method creates the calendar grid -- the middle pane.
     */
    private void initCenterPane() {
        //gets the current day/month/year
        LocalDate today = LocalDate.now();
        currentMonth = today.getMonth().name();
        intCurrentMonth = today.getMonthValue();
        currentDay = today.getDayOfMonth();
        currentYear = today.getYear();
        totDaysPerMonth = today.lengthOfMonth();
        
        editedMonth = currentMonth;
        intEditedMonth = intCurrentMonth;
        editedYear = currentYear;
        editedTotDaysPerMonth = totDaysPerMonth;
        
        //create the center pane
        calendarGridPane = new GridPane();
        calendarGridPane.setHgap(10.0);
        calendarGridPane.setVgap(10.0);
        calendarGridPane.setAlignment(Pos.CENTER);
        
        //month and year set up
        monthLabel = new Label(editedMonth + " " + editedYear);
        monthLabel.getStyleClass().add(CLASS_MONTH_LABEL);
        monthLabel.setAlignment(Pos.CENTER);
        monthLabel.setMinWidth(480);
        calendarGridPane.add(monthLabel, 0, 0, 7, 1);
        
        //contents of the center pane
        //init the days of the week labels
        sundayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.SUNDAY_LABEL, CLASS_SUBHEADING_LABEL, 0, 1, 1, 1);
        mondayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.MONDAY_LABEL, CLASS_SUBHEADING_LABEL, 1, 1, 1, 1);
        tuesdayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.TUESDAY_LABEL, CLASS_SUBHEADING_LABEL, 2, 1, 1, 1);
        wednesdayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.WEDNESDAY_LABEL, CLASS_SUBHEADING_LABEL, 3, 1, 1, 1);
        thursdayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.THURSDAY_LABEL, CLASS_SUBHEADING_LABEL, 4, 1, 1, 1);
        fridayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.FRIDAY_LABEL, CLASS_SUBHEADING_LABEL, 5, 1, 1, 1);
        saturdayLabel = initGui.initGridLabel(calendarGridPane, CAL_PropertyType.SATURDAY_LABEL, CLASS_SUBHEADING_LABEL, 6, 1, 1, 1);
        
        //init all the buttons
        buttonList = FXCollections.observableArrayList();
        button00 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 0, 3, 1, 1);
        button00.setId("00");
        buttonList.add(button00);
        button01 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 1, 3, 1, 1);
        button01.setId("01");
        buttonList.add(button01);
        button02 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 2, 3, 1, 1);
        button02.setId("02");
        buttonList.add(button02);
        button03 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 3, 3, 1, 1);
        button03.setId("03");
        buttonList.add(button03);
        button04 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 4, 3, 1, 1);
        button04.setId("04");
        buttonList.add(button04);
        button05 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 5, 3, 1, 1);
        button05.setId("05");
        buttonList.add(button05);
        button06 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 6, 3, 1, 1);
        button06.setId("06");
        buttonList.add(button06);
                
        button10 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 0, 4, 1, 1);
        button10.setId("10");
        buttonList.add(button10);
        button11 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 1, 4, 1, 1);
        button11.setId("11");
        buttonList.add(button11);
        button12 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 2, 4, 1, 1);
        button12.setId("12");
        buttonList.add(button12);
        button13 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 3, 4, 1, 1);
        button13.setId("13");
        buttonList.add(button13);
        button14 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 4, 4, 1, 1);
        button14.setId("14");
        buttonList.add(button14);
        button15 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 5, 4, 1, 1);
        button15.setId("15");
        buttonList.add(button15);
        button16 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 6, 4, 1, 1);
        button16.setId("16");
        buttonList.add(button16);
        
        button20 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 0, 5, 1, 1);
        button20.setId("20");
        buttonList.add(button20);
        button21 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 1, 5, 1, 1);
        button21.setId("21");
        buttonList.add(button21);
        button22 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 2, 5, 1, 1);
        button22.setId("22");
        buttonList.add(button22);
        button23 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 3, 5, 1, 1);
        button23.setId("23");
        buttonList.add(button23);
        button24 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 4, 5, 1, 1);
        button24.setId("24");
        buttonList.add(button24);
        button25 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 5, 5, 1, 1);
        button25.setId("25");
        buttonList.add(button25);
        button26 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 6, 5, 1, 1);
        button26.setId("26");
        buttonList.add(button26);
        
        button30 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 0, 6, 1, 1);
        button30.setId("30");
        buttonList.add(button30);
        button31 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 1, 6, 1, 1);
        button31.setId("31");
        buttonList.add(button31);
        button32 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 2, 6, 1, 1);
        button32.setId("32");
        buttonList.add(button32);
        button33 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 3, 6, 1, 1);
        button33.setId("33");
        buttonList.add(button33);
        button34 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 4, 6, 1, 1);
        button34.setId("34");
        buttonList.add(button34);
        button35 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 5, 6, 1, 1);
        button35.setId("35");
        buttonList.add(button35);
        button36 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 6, 6, 1, 1);
        button36.setId("36");
        buttonList.add(button36);
        
        button40 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 0, 7, 1, 1);
        button40.setId("40");
        buttonList.add(button40);
        button41 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 1, 7, 1, 1);
        button41.setId("41");
        buttonList.add(button41);
        button42 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 2, 7, 1, 1);
        button42.setId("42");
        buttonList.add(button42);
        button43 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 3, 7, 1, 1);
        button43.setId("43");
        buttonList.add(button43);
        button44 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 4, 7, 1, 1);
        button44.setId("44");
        buttonList.add(button44);
        button45 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 5, 7, 1, 1);
        button45.setId("45");
        buttonList.add(button45);
        button46 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 6, 7, 1, 1);
        button46.setId("46");
        buttonList.add(button46);
        
        button50 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 0, 8, 1, 1);
        button50.setId("50");
        buttonList.add(button50);
        button51 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 1, 8, 1, 1);
        button51.setId("51");
        buttonList.add(button51);
        button52 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 2, 8, 1, 1);
        button52.setId("52");
        buttonList.add(button52);
        button53 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 3, 8, 1, 1);
        button53.setId("53");
        buttonList.add(button53);
        button54 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 4, 8, 1, 1);
        button54.setId("54");
        buttonList.add(button54);
        button55 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 5, 8, 1, 1);
        button55.setId("55");
        buttonList.add(button55);
        button56 = initGui.initGridButton(calendarGridPane, CAL_PropertyType.DATE_BUTTON_TOOLTIP, 6, 8, 1, 1);
        button56.setId("56");
        buttonList.add(button56);
        
        prevMonthButton = initGui.initChildTooltipButton(calendarGridPane, CAL_PropertyType.PREV_TEXT, CAL_PropertyType.PREV_MONTH_TOOLTIP, 2, 10, 1, 1);
        prevMonthButton.setMinSize(50, 25);
        prevMonthButton.setTextAlignment(TextAlignment.CENTER);
        prevMonthButton.setAlignment(Pos.CENTER);
        nextMonthButton = initGui.initChildTooltipButton(calendarGridPane, CAL_PropertyType.NEXT_TEXT, CAL_PropertyType.NEXT_MONTH_TOOLTIP, 4, 10, 1, 1);
        nextMonthButton.setMinSize(50, 25);
        nextMonthButton.setTextAlignment(TextAlignment.CENTER);
        nextMonthButton.setAlignment(Pos.CENTER);
        
        changeMonthButton = initGui.initChildTooltipButton(calendarGridPane, CAL_PropertyType.CHANGE_MONTH_TEXT, CAL_PropertyType.CHANGE_MONTH_TOOLTIP, 0, 10, 1, 1);
        String[] changeMonthStringRegex = changeMonthButton.getText().split(" ");
        String changeMonthString = changeMonthStringRegex[0] + "\n" + changeMonthStringRegex[1];
        changeMonthButton.setText(changeMonthString);
        changeMonthButton.setMinSize(50, 25);
        changeMonthButton.setTextAlignment(TextAlignment.CENTER);
        
        changeYearButton = initGui.initChildTooltipButton(calendarGridPane, CAL_PropertyType.CHANGE_YEAR_TEXT, CAL_PropertyType.CHANGE_YEAR_TOOLTIP, 6, 10, 1, 1);
        String[] changeYearStringRegex = changeYearButton.getText().split(" ");
        String changeYearString = changeYearStringRegex[0] + "\n" + changeYearStringRegex[1];
        changeYearButton.setText(changeYearString);
        changeYearButton.setMinSize(50, 25);
        changeYearButton.setTextAlignment(TextAlignment.CENTER);
        
        //fill the table; included in this is setting the monthEventsTable items.
        calendarTableEvents();
        
        //and fill the current day table, if there is anything to fill
        dayEventsTable.setItems(eventController.handleChangeDayRequest());
    }
    
    /**
     * This method initializes the event handlers for everything in the GUI. This includes
     * all the file buttons, textfields, and calendar buttons.
     */
    private void initEventHandlers() {
        fileController = new FileController(messageDialog, yncDialog, calendarIO);
        newButton.setOnAction(e -> {
            fileController.handleNewCalendarRequest(this);
        });
        loadButton.setOnAction(e -> {
            fileController.handleLoadCalendarRequest(this);
        });
        saveButton.setOnAction(e -> {
            fileController.handleSaveCalendarRequest(this);
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });
        
        calController = new CalendarController(primaryStage, messageDialog, yncDialog, eventController, this);
        changeMonthButton.setOnAction(e -> {
            calController.handleChangeMonthRequest(this);
        });
        changeYearButton.setOnAction(e -> {
            calController.handleChangeYearRequest(this);
        });
        prevMonthButton.setOnAction(e -> {
            calController.handlePrevOrNextButton(this, 0);
        });
        nextMonthButton.setOnAction(e -> {
           calController.handlePrevOrNextButton(this, 1); 
        });
        button00.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button00);
        });
        button01.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button01);
        });
        button02.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button02);
        });
        button03.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button03);
        });
        button04.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button04);
        });
        button05.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button05);
        });
        button06.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button06);
        });
        button10.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button10);
        });
        button11.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button11);
        });
        button12.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button12);
        });
        button13.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button13);
        });
        button14.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button14);
        });
        button15.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button15);
        });
        button16.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button16);
        });
        button20.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button20);
        });
        button21.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button21);
        });
        button22.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button22);
        });
        button23.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button23);
        });
        button24.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button24);
        });
        button25.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button25);
        });
        button26.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button26);
        });
        button30.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button30);
        });
        button31.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button31);
        });
        button32.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button32);
        });
        button33.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button33);
        });
        button34.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button34);
        });
        button35.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button35);
        });
        button36.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button36);
        });
        button40.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button40);
        });
        button41.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button41);
        });
        button42.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button42);
        });
        button43.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button43);
        });
        button44.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button44);
        });
        button45.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button45);
        });
        button46.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button46);
        });
        button50.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button50);
        });
        button51.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button51);
        });
        button52.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button52);
        });
        button53.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button53);
        });
        button54.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button54);
        });
        button55.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button55);
        });
        button56.setOnAction(e -> {
            calController.handleDayEventsRequest(this, intEditedMonth, button56);
        });
    }
    
    /**
     * This method ends the thread by setting the running value to false.
     * Note: ONLY USED WHEN EXITING THE PROGRAM.
     */
    public void setRunningFalse() {
        running = false;
    }
    
    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved - Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        //can the calendar be saved?
        saveButton.setDisable(saved);
    }
    
    /**
     * This method helps get the events for all the data (like refreshing after every month)
     */
    public void calendarTableEvents() {
        getFirstDayOfMonth(intEditedMonth, editedYear);
        fillCalendarTable(editedTotDaysPerMonth);
    }
    /**
     * This method gets the first day of the week for the particular month and year.
     * 
     * @param month - the month we are using
     * @param year - the year we are using
     */
    public void getFirstDayOfMonth(int month, int year) {
        //create the calendar
        Calendar myCalendar = Calendar.getInstance();
        
        //set the calendar to last month and the first day
        if (month == 1)
            myCalendar.set(year - 1, 12, 1);
        else
            myCalendar.set(year, month - 1, 1);
        
        //gets the time (somehow moves the calendar to next month)
        Date firstDate = myCalendar.getTime();
        
        //gets the day of week (first three letters)
        DateFormat sdf = new SimpleDateFormat("EEE");
        String firstDayOfMonth = sdf.format(firstDate);
        
        //convert to numerical values
        if (firstDayOfMonth.equals(DOW_SUNDAY))
            monthStartDOW = 0;
        else if (firstDayOfMonth.equals(DOW_MONDAY))
            monthStartDOW = 1;
        else if (firstDayOfMonth.equals(DOW_TUESDAY))
            monthStartDOW = 2;
        else if (firstDayOfMonth.equals(DOW_WEDNESDAY))
            monthStartDOW = 3;
        else if (firstDayOfMonth.equals(DOW_THURSDAY))
            monthStartDOW = 4;
        else if (firstDayOfMonth.equals(DOW_FRIDAY))
            monthStartDOW = 5;
        else
            monthStartDOW = 6;
    }
    
    /**
     * This method fills the table by editing the buttons' text and abilities (border, etc.)
     * 
     * @param totDays - the total number of days in the viewing month
     */
    public void fillCalendarTable(int totDays) {
        //days
        int daysCompleted = 1;
        while (daysCompleted <= totDays) {
            //first row
            for (int i = monthStartDOW; i < 7; i++) {
                Button button = getButtonById(0, i);
                if (currentDay == daysCompleted && ((currentMonth.equals(editedMonth)) && (currentYear == editedYear))) {
                    currentDayButton = button;
                    button.getStyleClass().add(CLASS_TODAY_BUTTON);
                }
                button.setDisable(false);
                button.setText(daysCompleted + "");
                daysCompleted++;
            }
            //next rows through the end of the month
            for (int i = 1; i <= 5; i++) {
                for (int j = 0; j <= 6; j++) {
                    if (daysCompleted > totDays) 
                        break;
                    Button button = getButtonById(i, (j % 7));
                    if (currentDay == daysCompleted && ((currentMonth.equals(editedMonth)) && (currentYear == editedYear))) {
                        currentDayButton = button;
                        button.getStyleClass().add(CLASS_TODAY_BUTTON);
                    }
                    button.setDisable(false);
                    button.setText(daysCompleted + "");
                    daysCompleted++;
                }
            }
        }
        eventController.handleChangeMonthRequest(intEditedMonth);
        monthEventsTable.setItems(eventController.getMonthList());
    }
    
    /**
     * This method handles the time thread. It is ALWAYS running until the end of the
     * program, where the user MUST click the "Exit" button, or manually end it afterwards.
     */
    public void handleTimeThread() {
        //allows us to run this forever
        running = true;
        
        //get the task going
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                while(running) { //while we can still run the thread
                    //update the progress
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            LocalDate today = LocalDate.now();
                            currentMonth = today.getMonth().name();
                            intCurrentMonth = today.getMonthValue();
                            currentDay = today.getDayOfMonth();
                            currentYear = today.getYear();
                            totDaysPerMonth = today.lengthOfMonth();
                            
                            //get the time right now
                            LocalDateTime timeToday = LocalDateTime.now();
                            hour = timeToday.getHour();
                            minute = timeToday.getMinute();
                            int second = timeToday.getSecond();
                            String dayOfWeek = timeToday.getDayOfWeek().toString();
                            
                            String strHour = hour + "";
                            String strMin = minute + "";
                            String strSec = second + "";
                            
                            if (hour == 0) {
                                period = "AM";
                                strHour = "12";
                            }
                            else if (hour >= 1 && hour <= 11) {
                                period = "AM";
                            }
                            else if (hour >= 12) {
                                period = "PM";
                                strHour = (hour - 12) + "";
                            }
                            
                            if (minute <= 9)
                                strMin = "0" + minute;
                            if (second <= 9)
                                strSec = "0" + second;
                            timeLabel.setText(dayOfWeek + ", " + currentMonth + " " + currentDay + ", " + currentYear + "\n" + strHour + ":" + strMin + ":" + strSec + " " + period);
                            
                            //could be a new day
                            if (hour == 0) {
                                if ((currentDay == 1)  & (minute == 0) & (second == 0)) {
                                    //user is on previous month
                                    if ((intEditedMonth == (intCurrentMonth - 1) && (editedYear == currentYear))
                                            || ((intEditedMonth == 12 && intCurrentMonth == 1) && (editedYear == currentYear - 1))) {
                                        intEditedMonth = intCurrentMonth;
                                        editedYear = currentYear;
                                        editedTotDaysPerMonth = totDaysPerMonth;
                                        removeCurrentDayButton();
                                        disableAllButtons();
                                        calendarTableEvents();
                                        dayEventsTable.setItems(eventController.handleChangeDayRequest());
                                    }
                                    else if ((intEditedMonth == intCurrentMonth) && (editedYear == currentYear)) {   //user is on current (new) month
                                        updateCurrentDayButton();
                                        dayEventsTable.setItems(eventController.handleChangeDayRequest());
                                    }
                                }
                                else if ((minute == 0) & (second == 0)) {
                                    updateCurrentDayButton();
                                    dayEventsTable.setItems(eventController.handleChangeDayRequest());
                                }
                            }
                            
                            if (second == 0)
                                eventController.updateTodayCompleted();
                        }
                    });
                    
                    //sleep for one second
                    Thread.sleep(1000);
                }
                return null;
            }
        };
        
        Thread thread = new Thread(task);
        thread.start();
    }
    
    /**
     * This method sets the table lists, after loading and creating a new calendar.
     */
    public void setTheTables() {
        monthEventsTable.setItems(eventController.getMonthList());
        dayEventsTable.setItems(eventController.handleChangeDayRequest());
    }
    
    /**
     * This method gets the button's date (for the events table). Disregard the weird method name.
     * 
     * @param numRow - the row number of the date (usually 1 - 4)
     * @param dayOfWeek - the integer value of the day of the week, starting on Sunday (0 - 6)
     * @return the date from the button (usually between 1 and the month's total days)
     */
    public String getDateFromKnownButton(int numRow, int dayOfWeek) {
        int temp = 0;
        boolean areWeDone = false;
        String reDay = "";
        
        Button button;
        if (numRow == 1) {
            button = getButtonById(0, dayOfWeek);
            if (!button.isDisabled()) {
                reDay = button.getText();
                areWeDone = true;
            }
        }
        else if (numRow > 1) {
            button = getButtonById(0, dayOfWeek);
            if (!button.isDisabled())
                temp++;
        }
        
        for (int i = 1; i < 5; i++) {
            if (areWeDone)
                break;
            button = getButtonById(i, dayOfWeek);
            if (!button.isDisabled()) {
                temp++;
                if (temp == numRow) {
                    reDay = button.getText();
                    areWeDone = true;
                }
            }
        }
        return reDay;
    }
    
    /**
     * This method gets the date for tax day.
     * 
     * @return the date of tax day, either '15', '16', or '17'
     */
    public String getButtonForTaxDay() {
        String reDate = "";
        String buttonID;
        boolean areWeDone = false;
        
        //search for the button with the date
        for (int i = 0; i < buttonList.size(); i++) {
            if (areWeDone)
                break;
            if (!buttonList.get(i).isDisabled()) {
                reDate = buttonList.get(i).getText();
                if (reDate.equals("15")) {                                  //found it!
                    buttonID = buttonList.get(i).getId();
                    if (buttonID.endsWith("6") || buttonID.endsWith("0")) { //if it's on a weekend
                        buttonID = buttonList.get(i + 1).getId();
                        if (buttonID.endsWith("0")) {                         //sunday!
                            reDate = buttonList.get(i + 2).getText();       //get the monday's date
                        }
                        else {                                               //it's on a monday!
                            reDate = buttonList.get(i + 1).getText();       //get it
                        }
                    }
                    areWeDone = true;                                       //this means we use the 15th
                }
            }
        }
        return reDate;
    }
    
    /**
     * This method gets the date for election day.
     * 
     * @return the date for election day, between (2 - 7)
     */
    public String getButtonForElectionDay() {
        String reDay;
        Button button = getButtonById(0, 1);
        if (!button.isDisabled())
            reDay = getButtonById(0, 2).getText();
        else
            reDay = getButtonById(1, 2).getText();
        return reDay;
    }
    
    /**
     * This method gets the date of the button from the last week of the month
     * 
     * @param dayOfWeek - the day of the week, starting on Sunday (0 - 6)
     * @return the date, usually between (20 - 31)
     */
    public String getLastDateFromKnownButton(int dayOfWeek) {
        boolean areWeDone = false;
        String reDay = "";
        
        Button button;
        for (int i = 5; i > 0; i--) {
            if (areWeDone)
                break;
            button = getButtonById(i, dayOfWeek);
            if (!button.isDisabled()) {
                reDay = button.getText();
                areWeDone = true;
            }
        }
        return reDay;
    }
    
    /**
     * This method gets the seasons! 0 for Spring, 1 for Summer, 2 for Fall/Autumn, 3 for Winter.
     * 
     * @param season - the season we are looking up
     * @return the date of when the season changes
     */
    public String getChangeSeasons(int season) {
        if (season == 0)
            return "3/20";
        else if (season == 1) {
            if (editedYear % 4 == 0)
                return "6/20";
            else
                return "6/21";
        }
        else if (season == 2) {
            if (editedYear % 4 == 0 || editedYear % 4 == 1)
                return "9/22";
            else
                return "9/23";
        }
        else {
            if (editedYear % 4 == 3)
                return "12/22";
            else
                return "12/21";
        }
    }
    
    /**
     * This method places special value in holiday or event days by editing the button's styleclass.
     * 
     * @param list the list of events for the current viewing month
     */
    public void setHolidayEventsCalendar(ObservableList list) {
        for (Object list1 : list) {
            Event e = (Event)list1;
            boolean run = false;
            Button b;
            for (Button buttonList1 : buttonList) {
                if (run)
                    break;
                String date = buttonList1.getText();
                String[] findE = e.getDate().split("/");
                if (findE[1].equals(date)) {
                    if (e.getIsHoliday())
                        buttonList1.getStyleClass().add(CLASS_EVENTS_HOLIDAY);
                    else {
                        if (!buttonList1.getStyleClass().contains(CLASS_EVENTS_REGULAR))
                            buttonList1.getStyleClass().add(CLASS_EVENTS_REGULAR);
                    }
                    run = true;
                }
            }
        }
    }
    
    /**
     * This method updates the month label on top of the calendar.
     */
    public void updateMonthLabel() {
        monthLabel.setText(editedMonth + " " + editedYear);
    }
    
    /**
     * This method updates the current day button (the highlighted button) when we go to a new day.
     * Note: this only works if we are currently viewing the the current month.
     */
    public void updateCurrentDayButton() {
        if (!(editedMonth.equals(currentMonth) && editedYear == currentYear))
            return;
        boolean run = true;
        for (int i = 0; i < buttonList.size(); i++) {
            if (!run)
                break;
            if (currentDayButton == buttonList.get(i)) {
                Button button1 = buttonList.get(i);
                Button button2 = buttonList.get(i + 1);
                button1.getStyleClass().remove(CLASS_TODAY_BUTTON);
                button2.getStyleClass().add(CLASS_TODAY_BUTTON);
                currentDayButton = button2;
                run = false;
            }
        }
    }
    
    /**
     * This method removes the stylesheet from the previous current day (only for a change of months).
     */
    public void removeCurrentDayButton() {
        boolean run = true;
        for (Button button : buttonList) {
            if (!run)
                break;
            if (currentDayButton == button) {
                Button but = button;
                but.getStyleClass().remove(CLASS_TODAY_BUTTON);
                run = false;
            }
        }
    }
    
    /**
     * This method disables all buttons (only for a change of months). Usually this is
     * used so we can fill the buttons again.
     */
    public void disableAllButtons() {
        for (Button button : buttonList) {
            button.setText("");
            if (button.getStyleClass().contains(CLASS_EVENTS_REGULAR))
                button.getStyleClass().remove(CLASS_EVENTS_REGULAR);
            if (button.getStyleClass().contains(CLASS_EVENTS_HOLIDAY))
                button.getStyleClass().remove(CLASS_EVENTS_HOLIDAY);
            button.setDisable(true);
        }
    }
    
    /**
     * This method gets the string of the correct month and total days, from the integer
     * value of the month.
     * 
     * @param intMonth - the integer value of the month
     * @param year - the year
     * @return the string value of the month
     */
    public String getMonth(int intMonth, int year) {
        if (intMonth == 1) {
            editedTotDaysPerMonth = 31;
            return M_JANUARY;
        }
        else if (intMonth == 2) {
            //leap year
            if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)))
                editedTotDaysPerMonth = 29;
            else
                editedTotDaysPerMonth = 28;
            return M_FEBRUARY;
        }
        else if (intMonth == 3) {
            editedTotDaysPerMonth = 31;
            return M_MARCH;
        }
        else if (intMonth == 4) {
            editedTotDaysPerMonth = 30;
            return M_APRIL;
        }
        else if (intMonth == 5) {
            editedTotDaysPerMonth = 31;
            return M_MAY;
        }
        else if (intMonth == 6) {
            editedTotDaysPerMonth = 30;
            return M_JUNE;
        }
        else if (intMonth == 7) {
            editedTotDaysPerMonth = 31;
            return M_JULY;
        }
        else if (intMonth == 8) {
            editedTotDaysPerMonth = 31;
            return M_AUGUST;
        }
        else if (intMonth == 9) {
            editedTotDaysPerMonth = 30;
            return M_SEPTEMBER;
        }
        else if (intMonth == 10) {
            editedTotDaysPerMonth = 31;
            return M_OCTOBER;
        }
        else if (intMonth == 11) {
            editedTotDaysPerMonth = 30;
            return M_NOVEMBER;
        }
        else {
            editedTotDaysPerMonth = 31;
            return M_DECEMBER;
        }
    }
    
    /**
     * This method gets the correct month (integer, 1 - 12) and the total days, 
     * from the string value of the month.
     * 
     * @param month - the string value of the month
     * @return the integer value of the month
     */
    public int getMonth(String month) {
        if (month.equals(M_JANUARY)) {
            editedTotDaysPerMonth = 31;
            return 1;
        }
        else if (month.equals(M_FEBRUARY)) {
            //leap year
            if ((editedYear % 400 == 0) || ((editedYear % 100 != 0) && (editedYear % 4 == 0)))
                editedTotDaysPerMonth = 29;
            else
                editedTotDaysPerMonth = 28;
            return 2;
        }
        else if (month.equals(M_MARCH)) {
            editedTotDaysPerMonth = 31;
            return 3;
        }
        else if (month.equals(M_APRIL)) {
            editedTotDaysPerMonth = 30;
            return 4;
        }
        else if (month.equals(M_MAY)) {
            editedTotDaysPerMonth = 31;
            return 5;
        }
        else if (month.equals(M_JUNE)) {
            editedTotDaysPerMonth = 30;
            return 6;
        }
        else if (month.equals(M_JULY)) {
            editedTotDaysPerMonth = 31;
            return 7;
        }
        else if (month.equals(M_AUGUST)) {
            editedTotDaysPerMonth = 31;
            return 8;
        }
        else if (month.equals(M_SEPTEMBER)) {
            editedTotDaysPerMonth = 30;
            return 9;
        }
        else if (month.equals(M_OCTOBER)) {
            editedTotDaysPerMonth = 31;
            return 10;
        }
        else if (month.equals(M_NOVEMBER)) {
            editedTotDaysPerMonth = 30;
            return 11;
        }
        else {
            editedTotDaysPerMonth = 31;
            return 12;
        }
    }
    
    /**
     * This method gets the correct button from the list. We multiply by 7 to get the
     * correct row, then add with the second parameter to get the correct button.
     * 
     * @param i - the button row (0 - 5)
     * @param j - the button column (0 - 6)
     * @return the button we want
     */
    public Button getButtonById(int i, int j) {
        int placement = (i * 7) + j;
        Button button = buttonList.get(placement);
        return button;
    }
}