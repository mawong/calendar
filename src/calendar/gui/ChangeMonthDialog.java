package calendar.gui;

import calendar.CAL_PropertyType;
import static calendar.gui.CAL_GUI.CLASS_HEADING_LABEL;
import static calendar.gui.CAL_GUI.PRIMARY_STYLE_SHEET;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This is the dialog box to go to a different month in the specified year (changeMonthButton).
 * 
 * @author MAVIRI
 */
public class ChangeMonthDialog extends Stage {
    //controls for the UI
    GridPane gridPane;
    Scene dialogScene;
    Label changeMonthLabel;
    ToggleGroup group;
    RadioButton janButton;
    RadioButton febButton;
    RadioButton marButton;
    RadioButton aprButton;
    RadioButton mayButton;
    RadioButton junButton;
    RadioButton julButton;
    RadioButton augButton;
    RadioButton sepButton;
    RadioButton octButton;
    RadioButton novButton;
    RadioButton decButton;
    Button completeButton;
    Button cancelButton;
    
    //keep track which button is pressed
    String selection;
    String month;
    
    //constants
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    
    /**
     * Constructor that creates the change month dialog box.
     * 
     * @param primaryStage - the current window that the user is looking at
     * @param messageDialog - the message dialog
     */
    public ChangeMonthDialog(Stage primaryStage, MessageDialog messageDialog) {
        //make it modal (others will wait when this is displayed)
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //create the container
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        //set the title
        setTitle(props.getProperty(CAL_PropertyType.CHANGE_MONTH_TITLE));
        
        //put the heading on the grid
        changeMonthLabel = new Label(props.getProperty(CAL_PropertyType.CHANGE_MONTH_LABEL));
        changeMonthLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //now the radio buttons
        group = new ToggleGroup();
        janButton = new RadioButton(CAL_GUI.M_JANUARY);
        janButton.setToggleGroup(group);
        febButton = new RadioButton(CAL_GUI.M_FEBRUARY);
        febButton.setToggleGroup(group);
        marButton = new RadioButton(CAL_GUI.M_MARCH);
        marButton.setToggleGroup(group);
        aprButton = new RadioButton(CAL_GUI.M_APRIL);
        aprButton.setToggleGroup(group);
        mayButton = new RadioButton(CAL_GUI.M_MAY);
        mayButton.setToggleGroup(group);
        junButton = new RadioButton(CAL_GUI.M_JUNE);
        junButton.setToggleGroup(group);
        julButton = new RadioButton(CAL_GUI.M_JULY);
        julButton.setToggleGroup(group);
        augButton = new RadioButton(CAL_GUI.M_AUGUST);
        augButton.setToggleGroup(group);
        sepButton = new RadioButton(CAL_GUI.M_SEPTEMBER);
        sepButton.setToggleGroup(group);
        octButton = new RadioButton(CAL_GUI.M_OCTOBER);
        octButton.setToggleGroup(group);
        novButton = new RadioButton(CAL_GUI.M_NOVEMBER);
        novButton.setToggleGroup(group);
        decButton = new RadioButton(CAL_GUI.M_DECEMBER);
        decButton.setToggleGroup(group);
        
        //and the control buttons
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        //register event handlers for all buttons
        EventHandler monthHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
          RadioButton sourceButton = (RadioButton)ae.getSource();
          ChangeMonthDialog.this.month = sourceButton.getText();
        };
                
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            ChangeMonthDialog.this.selection = sourceButton.getText();
            if (this.month != null || this.selection.equals(CANCEL))
                ChangeMonthDialog.this.hide();
            else {
                messageDialog.show(props.getProperty(CAL_PropertyType.NO_MONTH_CHOICE_MESSAGE));
            }
        };
        
        janButton.setOnAction(monthHandler);
        febButton.setOnAction(monthHandler);
        marButton.setOnAction(monthHandler);
        aprButton.setOnAction(monthHandler);
        mayButton.setOnAction(monthHandler);
        junButton.setOnAction(monthHandler);
        julButton.setOnAction(monthHandler);
        augButton.setOnAction(monthHandler);
        sepButton.setOnAction(monthHandler);
        octButton.setOnAction(monthHandler);
        novButton.setOnAction(monthHandler);
        decButton.setOnAction(monthHandler);
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //now, arrange everything all at once
        gridPane.add(changeMonthLabel, 0, 0, 4, 1);
        gridPane.add(janButton, 0, 3, 1, 1);
        gridPane.add(febButton, 1, 3, 1, 1);
        gridPane.add(marButton, 2, 3, 1, 1);
        gridPane.add(aprButton, 3, 3, 1, 1);
        gridPane.add(mayButton, 0, 4, 1, 1);
        gridPane.add(junButton, 1, 4, 1, 1);
        gridPane.add(julButton, 2, 4, 1, 1);
        gridPane.add(augButton, 3, 4, 1, 1);
        gridPane.add(sepButton, 0, 5, 1, 1);
        gridPane.add(octButton, 1, 5, 1, 1);
        gridPane.add(novButton, 2, 5, 1, 1);
        gridPane.add(decButton, 3, 5, 1, 1);
        gridPane.add(completeButton, 0, 8, 1, 1);
        gridPane.add(cancelButton, 1, 8, 1, 1);
        
        //and put the grid pane in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * This accessor method gets the selection the user made.
     * 
     * @return the selection the user made
     */
    public String getSelection() {
        return selection;
    }
    
    /**
     * This method gets the month the user wants.
     * 
     * @return the month the user wants
     */
    public String getMonth() {
        return month;
    }
    
    /**
     * This method opens and shows the change month dialog box.
     */
    public void showChangeMonthDialog() {
        //open up the dialog
        this.showAndWait();
    }
    
    /**
     * This method checks to see if complete was selected.
     * 
     * @return true if complete was selected, false otherwise
     */
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
}