package calendar.gui;

import calendar.CAL_PropertyType;
import static calendar.gui.CAL_GUI.CLASS_HEADING_LABEL;
import static calendar.gui.CAL_GUI.CLASS_SUBHEADING_LABEL;
import static calendar.gui.CAL_GUI.PRIMARY_STYLE_SHEET;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class represents a dialog that opens when the user clicks the "Change Year"
 * button.  It allows the user to change to a specified year in time (only allow 4 digits),
 * or allows a user to go to a specified year by typing in a valid year.
 *
 * @author MAVIRI
 */
public class ChangeYearDialog extends Stage {
    //controls for the UI
    GridPane gridPane;
    Scene dialogScene;
    Label changeYearLabel;
    Label year1TextLabel;
    ComboBox yearCenturiesComboBox;
    ComboBox yearDecadesComboBox;
    ChoiceBox yearChoiceChoiceBox;
    Label orLabel;
    Label year2TextLabel;
    TextField yearTextField;
    RadioButton option1Button;
    RadioButton option2Button;
    Button completeButton;
    Button cancelButton;
    
    //keep track of which button is pressed/what year user entered
    String selection;
    int yearChoice;
    int centuryChoice;
    int decadeChoice;
    String radioButtonChoice;
    String yearTF;
    
    //constants
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String YEAR1_TEXT_LABEL = "1. Change Year\n(Limit 500 Years): ";
    public static final String OR_LABEL = "OR";
    public static final String YEAR2_TEXT_LABEL = "2. Change Year\n(Limit 4-5 Digits): ";
    public static final String OPTION1_BUTTON = "Option 1";
    public static final String OPTION2_BUTTON = "Option 2";
    public static final String CENTURY1 = "1500 - 1599";
    public static final String CENTURY2 = "1600 - 1699";
    public static final String CENTURY3 = "1700 - 1799";
    public static final String CENTURY4 = "1800 - 1899";
    public static final String CENTURY5 = "1900 - 1999";
    public static final String CENTURY6 = "2000 - 2099";
    public static final String CENTURY7 = "2100 - 2199";
    public static final String CENTURY8 = "2200 - 2299";
    public static final String CENTURY9 = "2300 - 2399";
    public static final String CENTURY10 = "2400 - 2499";
    
    /**
     * Constructor that creates the change year dialog box.
     * 
     * @param primaryStage - the window that the user is currently looking at
     * @param messageDialog - the message dialog
     */
    public ChangeYearDialog(Stage primaryStage, MessageDialog messageDialog) {
        //make it modal (others will wait when this is displayed)
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //create the container
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        //set the title
        setTitle(props.getProperty(CAL_PropertyType.CHANGE_YEAR_TITLE));
        
        //put the heading on the grid
        changeYearLabel = new Label(props.getProperty(CAL_PropertyType.CHANGE_YEAR_LABEL));
        changeYearLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //now init the textLabel and boxes
        year1TextLabel = new Label(YEAR1_TEXT_LABEL);
        yearCenturiesComboBox = new ComboBox();
        yearCenturiesComboBox.getItems().addAll(CENTURY1, CENTURY2, CENTURY3, CENTURY4, CENTURY5,
                                                CENTURY6, CENTURY7, CENTURY8, CENTURY9, CENTURY10);
        yearDecadesComboBox = new ComboBox();
        yearDecadesComboBox.setMinWidth(110);
        yearDecadesComboBox.setDisable(true);
        yearChoiceChoiceBox = new ChoiceBox();
        yearChoiceChoiceBox.setMinWidth(60);
        yearChoiceChoiceBox.setDisable(true);
        
        //and the orLabel and year2Label
        orLabel = new Label(OR_LABEL);
        orLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        year2TextLabel = new Label(YEAR2_TEXT_LABEL);
        
        //init the textfield
        yearTextField = new TextField();
        yearTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            yearTF = yearTextField.textProperty().getValue();
        });
        
        //now init the radio buttons
        ToggleGroup group = new ToggleGroup();
        option1Button = new RadioButton(OPTION1_BUTTON);
        option1Button.setToggleGroup(group);
        option2Button = new RadioButton(OPTION2_BUTTON);
        option2Button.setToggleGroup(group);
        
        //and the buttons
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        //register event handlers for the boxes and buttons
        yearCenturiesComboBox.setOnAction(e -> {
            if (yearDecadesComboBox.isDisable())
                yearDecadesComboBox.setDisable(false);
            if (!yearDecadesComboBox.getItems().isEmpty())
                clearDecadesComboBox();
            String intermediateCentury = yearCenturiesComboBox.getSelectionModel().getSelectedItem().toString();
            centuryChoice = Integer.parseInt(intermediateCentury.substring(0, 4));
            loadDecadesComboBox();
        });
        
        yearDecadesComboBox.setOnAction(e -> {
            if (yearChoiceChoiceBox.isDisable()) {
                yearChoiceChoiceBox.setDisable(false);
            }
            if (!yearChoiceChoiceBox.getItems().isEmpty())
                clearYearsChoiceBox();
            if (!yearDecadesComboBox.getItems().isEmpty()) {
                String intermediateDecade = yearDecadesComboBox.getSelectionModel().getSelectedItem().toString();
                decadeChoice = Integer.parseInt(intermediateDecade.substring(0, 4));
            }
            loadYearsChoiceBox();
        });
        
        yearChoiceChoiceBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            yearChoice = decadeChoice + newValue.intValue();
        });
        
        EventHandler yearHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
          RadioButton sourceButton = (RadioButton)ae.getSource();
          ChangeYearDialog.this.radioButtonChoice = sourceButton.getText();
          if (sourceButton.getText().equals(OPTION1_BUTTON)) {
              if (!yearChoiceChoiceBox.isDisabled() && (yearChoiceChoiceBox.getSelectionModel().getSelectedIndex() >= 0))
                  yearChoice = decadeChoice + yearChoiceChoiceBox.getSelectionModel().getSelectedIndex();
              else
                  yearChoice = 0;
          }
          else
            yearChoice = 0;
        };
        option1Button.setOnAction(yearHandler);
        option2Button.setOnAction(yearHandler);
        
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            ChangeYearDialog.this.selection = sourceButton.getText();
            if (this.selection.equals(CANCEL))
                ChangeYearDialog.this.hide();
            else {
                boolean good = checkForValidity(messageDialog, props);
                if (good)
                    ChangeYearDialog.this.hide();
            }
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //now, arrange everything on the grid
        gridPane.add(changeYearLabel, 0, 0, 5, 1);
        gridPane.add(year1TextLabel, 0, 3, 1, 1);
        gridPane.add(yearCenturiesComboBox, 1, 3, 1, 1);
        gridPane.add(yearDecadesComboBox, 2, 3, 1, 1);
        gridPane.add(yearChoiceChoiceBox, 3, 3, 1, 1);
        gridPane.add(orLabel, 1, 5, 1, 1);
        gridPane.add(year2TextLabel, 0, 7, 1, 1);
        gridPane.add(yearTextField, 1, 7, 3, 1);
        gridPane.add(option1Button, 0, 9, 1, 1);
        gridPane.add(option2Button, 1, 9, 1, 1);
        gridPane.add(completeButton, 0, 12, 1, 1);
        gridPane.add(cancelButton, 1, 12, 1, 1);
        
        //and put the dialog scene in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * This method gets the selection the user pressed.
     * 
     * @return the selection the user pressed
     */
    public String getSelection() {
        return selection;
    }
    
    /**
     * This method gets the year the user wants.
     * 
     * @return the year the user wants
     */
    public int getYear() {
        return yearChoice;
    }
    
    /**
     * This method shows the change year dialog box.
     */
    public void showChangeYearDialog() {
        //open the dialog
        this.showAndWait();
    }
    
    /**
     * This method checks to see if complete was selected.
     * 
     * @return true if complete was selected, false otherwise
     */
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    /**
     * This method loads the decades combo box based on the century combo box.
     */
    private void loadDecadesComboBox() {
        int temp = centuryChoice;
        for (int i = 0; i <= 9; i++) {
            yearDecadesComboBox.getItems().add(temp + " - " + (temp+9));
            temp += 10;
        }
    }
    
    /**
     * This method loads the years choice box based on the decades combo box.
     */
    private void loadYearsChoiceBox() {
        int temp = decadeChoice;
        for (int i = 0; i <= 9; i++) {
            yearChoiceChoiceBox.getItems().add(temp);
            temp += 1;
        }
    }
    
    /**
     * This method clears the decades combo box.
     */
    private void clearDecadesComboBox() {
        yearDecadesComboBox.getItems().clear();
        decadeChoice = 0;
        clearYearsChoiceBox();
    }
    
    /**
     * This method clears the years choice box.
     */
    private void clearYearsChoiceBox() {
        yearChoiceChoiceBox.getItems().clear();
        yearChoice = 0;
    }
    
    /**
     * This method checks to see if the years were entered correctly. If they weren't, we let the
     * user know from a messageDialog and return false. Otherwise, we're good, so return true.
     * 
     * @param messageDialog - the message dialog
     * @param props - the properties manager for the messages
     * @return true if the year is valid, false otherwise
     */
    private boolean checkForValidity(MessageDialog messageDialog, PropertiesManager props) {
        if (radioButtonChoice == null) {
            messageDialog.show(props.getProperty(CAL_PropertyType.NO_YEAR_OPTION_CHOICE_MESSAGE));
            return false;
        }
        else {
            if (radioButtonChoice.equals(OPTION1_BUTTON)) {
                if (yearChoice == 0) {
                    messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_YEAR_CHOICE_MESSAGE));
                    return false;
                }
                else
                    return true;
            }
            else {
                if (yearTF == null || yearTF.equals("") || (yearTF.length() < 4 || yearTF.length() > 5)) {
                    messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_YEAR_CHOICE_MESSAGE));
                    return false;
                }
                else {
                    for (int i = 0; i < yearTF.length(); i++) {
                        char ch = yearTF.charAt(i);
                        if (!Character.isDigit(ch)) {
                            messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_YEAR_CHOICE_MESSAGE));
                            return false;
                        }
                    }
                    yearChoice = Integer.parseInt(yearTF);
                }
            }
        }
        return true;
    }
}