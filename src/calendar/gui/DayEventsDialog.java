package calendar.gui;

import calendar.CAL_PropertyType;
import calendar.controller.EventController;
import calendar.data.Event;
import static calendar.gui.CAL_GUI.CLASS_EVENTS_REGULAR;
import static calendar.gui.CAL_GUI.CLASS_HEADING_LABEL;
import static calendar.gui.CAL_GUI.CLASS_SUBHEADING_LABEL;
import static calendar.gui.CAL_GUI.PRIMARY_STYLE_SHEET;
import java.util.Collections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class represents a dialog that pops up when the user clicks a button on the
 * calendar.
 *
 * @author MAVIRI
 */
public class DayEventsDialog extends Stage {
    //GUI, dialog, and controller
    CAL_GUI gui;
    AddEditRemoveEventDialog aered;
    EditOrRemoveChoiceDialog eorcd;
    YesNoCancelDialog yncd;
    EventController hc;
    
    //controls for the UI
    GridPane gridPane;
    Scene dialogScene;
    Label dateLabel;
    Label eventsLabel;
    Label listOfEventsLabel;
    Button addButton;
    Button editButton;
    Button removeButton;
    Button okButton;
    Button eventButton;
    
    //keep track of the date
    int month;
    String day;
    int year;
    
    //constants
    public static final String OK = "OK";
    final String M_JANUARY = "JANUARY";
    final String M_FEBRUARY = "FEBRUARY";
    final String M_MARCH = "MARCH";
    final String M_APRIL = "APRIL";
    final String M_MAY = "MAY";
    final String M_JUNE = "JUNE";
    final String M_JULY = "JULY";
    final String M_AUGUST = "AUGUST";
    final String M_SEPTEMBER = "SEPTEMBER";
    final String M_OCTOBER = "OCTOBER";
    final String M_NOVEMBER = "NOVEMBER";
    final String M_DECEMBER = "DECEMBER";
    
    /**
     * Constructor that initializes the dialog.
     * 
     * @param primaryStage - the stage we will place the dialog in
     * @param messageDialog - the message dialog
     * @param initGUI - the GUI we are using
     * @param hc - the hash controller sent for us
     * @param yncDialog - the yes/no/cancel dialog
     */
    public DayEventsDialog(Stage primaryStage, MessageDialog messageDialog, CAL_GUI initGUI, EventController hc, YesNoCancelDialog yncDialog) {
        aered = new AddEditRemoveEventDialog(primaryStage, messageDialog, initGUI);
        eorcd = new EditOrRemoveChoiceDialog(primaryStage, messageDialog, hc, this);
        gui = initGUI;
        this.hc = hc;
        yncd = yncDialog;
        
        //make it modal (others will wait when this is displayed)
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //create the container
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        //set the title
        setTitle(props.getProperty(CAL_PropertyType.EVENTS_DIALOG_TITLE));
        
        //put the date and heading on the grid
        dateLabel = new Label();
        dateLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        eventsLabel = new Label(props.getProperty(CAL_PropertyType.EVENTS_DIALOG_LABEL));
        eventsLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        
        //now init the label for the list of labels
        listOfEventsLabel = new Label();
        listOfEventsLabel.setMinSize(100, 200);
        listOfEventsLabel.setAlignment(Pos.TOP_LEFT);
        
        //now the events buttons
        addButton = new Button(props.getProperty(CAL_PropertyType.ADD_EVENT_TEXT));
        Tooltip addButtonTooltip = new Tooltip(props.getProperty(CAL_PropertyType.ADD_EVENT_TOOLTIP));
        addButton.setTooltip(addButtonTooltip);
        addButton.setMinSize(50, 25);
        editButton = new Button(props.getProperty(CAL_PropertyType.EDIT_EVENT_TEXT));
        Tooltip editButtonTooltip = new Tooltip(props.getProperty(CAL_PropertyType.EDIT_EVENT_TOOLTIP));
        editButton.setTooltip(editButtonTooltip);
        editButton.setMinSize(50, 25);
        removeButton = new Button(props.getProperty(CAL_PropertyType.REMOVE_EVENT_TEXT));
        Tooltip removeButtonTooltip = new Tooltip(props.getProperty(CAL_PropertyType.REMOVE_EVENT_TOOLTIP));
        removeButton.setTooltip(removeButtonTooltip);
        removeButton.setMinSize(50, 25);
        
        //and the control button
        okButton = new Button(OK);
        okButton.setMinSize(50, 25);
        
        //register event handlers for the buttons
        EventHandler addEditRemoveHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            if (sourceButton == addButton)
                registerAddEditRemoveButtons(0);
            else if (sourceButton == editButton)
                registerAddEditRemoveButtons(1);
            else
                registerAddEditRemoveButtons(2);
        };
        addButton.setOnAction(addEditRemoveHandler);
        editButton.setOnAction(addEditRemoveHandler);
        removeButton.setOnAction(addEditRemoveHandler);
        
        EventHandler okHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            DayEventsDialog.this.hide();
        };
        okButton.setOnAction(okHandler);
        
        //now, arrange everything onto the grid
        gridPane.add(dateLabel, 0, 0, 4, 1);
        gridPane.add(eventsLabel, 0, 2, 2, 1);
        gridPane.add(listOfEventsLabel, 0, 3, 12, 1);
        gridPane.add(addButton, 1, 4, 1, 1);
        gridPane.add(editButton, 3, 4, 1, 1);
        gridPane.add(removeButton, 5, 4, 1, 1);
        gridPane.add(okButton, 3, 6, 1, 1);
        
        //and put the dialog scene in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * This method shows the day events dialog box.
     * 
     * @param month - the viewing month
     * @param year - the viewing year
     * @param getDate - the button that the user clicked (to get the date)
     */
    public void showDayEventsDialog(int month, int year, Button getDate) {
        this.month = month;
        this.year = year;
        day = getDate.getText();
        eventButton = getDate;
        
        //set the label texts
        dateLabel.setText(month + "/" + getDate.getText() + "/" + year);
        fillEventsLabel();
        this.showAndWait();
    }
    
    /**
     * This method gets the month as a string value
     * 
     * @param month - the integer value of the month
     * @return the string value of the month
     */
    private String getStringMonth(int month) {
        if (month == 1)
            return M_JANUARY;
        else if (month == 2)
            return M_FEBRUARY;
        else if (month == 3)
            return M_MARCH;
        else if (month == 4)
            return M_APRIL;
        else if (month == 5)
            return M_MAY;
        else if (month == 6)
            return M_JUNE;
        else if (month == 7)
            return M_JULY;
        else if (month == 8)
            return M_AUGUST;
        else if (month == 9)
            return M_SEPTEMBER;
        else if (month == 10)
            return M_OCTOBER;
        else if (month == 11)
            return M_NOVEMBER;
        else
            return M_DECEMBER;
    }
    
    /**
     * This method fills the middle label with the list of events for that day. It also disables
     * the edit and remove buttons if there are no events that the user made on that day. It also
     * disables the add button if the day is in the past.
     */
    private void fillEventsLabel() {
        listOfEventsLabel.setText("");
        ObservableList monthList = hc.getListForCurrentMonth(getStringMonth(month));
        String[] str;
        int listSize = 1;
        boolean disableButtons = true;
        for (Object obj : monthList) {
            Event e = (Event)obj;
            str = e.getDate().split("/");
            if (day.equals(str[1])) {
                listOfEventsLabel.setText(listOfEventsLabel.getText() + listSize + ":\t" + e.getStartTime()
                + "\t\t" + e.getActivity() + "\n");
                listSize++;
                if (!e.getIsPreset())
                    disableButtons = false;
            }
        }
        setTheButtons(disableButtons);
    }
    
    /**
     * This method sets the add, edit, and remove buttons to disabled or enabled
     * 
     * @param isDisable - the boolean to set if the edit and remove buttons should be disabled.
     */
    private void setTheButtons(boolean isDisable) {
        editButton.setDisable(isDisable);
        removeButton.setDisable(isDisable);
        if (year < gui.getCurrentYear())
            addButton.setDisable(true);
        else if (year == gui.getCurrentYear()) {
            if (month < gui.getIntCurrentMonth())
                addButton.setDisable(true);
            else if (month == gui.getIntCurrentMonth()) {
                if (Integer.parseInt(day) < gui.getCurrentDay())
                    addButton.setDisable(true);
                else
                    addButton.setDisable(false);
            }
            else
                addButton.setDisable(false);
        }
        else
            addButton.setDisable(false);
    }
    
    /**
     * This method handles the registers the buttons, and allows us to do whatever we want 
     * with adding/editing/removing events.
     * 
     * @param buttonNum - the number for the button pressed (0 = add, 1 = edit, 2 = remove)
     */
    private void registerAddEditRemoveButtons(int buttonNum) {
        if (buttonNum == 0) {                           //add an event
            aered.showAddEventDialog(month, day, year);
            
            //did the user confirm?
            if (aered.wasCompleteSelected()) {
                //get the event
                Event e = aered.getEvent();
                
                //and add it to the list
                hc.addEventToList(e);
                hc.getHashValues().getAllNewEvents().add(e);
                Collections.sort(hc.getHashValues().getAllNewEvents());
                
                //and add it to the list events label
                fillEventsLabel();
                
                //and highlight the day (if it isn't already highlighted)
                if (!eventButton.getStyleClass().contains(CLASS_EVENTS_REGULAR))
                eventButton.getStyleClass().add(CLASS_EVENTS_REGULAR);
                
                //if we added an event to today, update the table
                if (month == gui.getIntCurrentMonth() && Integer.parseInt(day) == gui.getCurrentDay() && year == gui.getCurrentYear())
                    gui.dayEventsTable.setItems(hc.handleChangeDayRequest());
                
                //and mark it as edited
                gui.getFileController().markAsEdited(gui);
            }
        }
        else if (buttonNum == 1) {
            eorcd.showEditChoiceDialog();
            
            //did the user confirm?
            if (eorcd.wasCompleteSelected()) {
                //get the event
                Event e = eorcd.getEvent();
                aered.showEditEventDialog(month, day, year, e);
                
                //did the user confirm?
                if (aered.wasCompleteSelected()) {
                    //get the month's list to remove the previous event
                    hc.getMonthList().remove(e);
                    hc.getHashValues().getAllNewEvents().remove(e);
                    Collections.sort(hc.getHashValues().getAllNewEvents());
                    
                    //get the event
                    e = aered.getEvent();
                    
                    //and add it to the list
                    hc.addEventToList(e);
                    hc.getHashValues().getAllNewEvents().add(e);
                    Collections.sort(hc.getHashValues().getAllNewEvents());
                    
                    //update the events label
                    fillEventsLabel();
                    
                    //and update the table if it was from today
                    if (month == gui.getIntCurrentMonth() && Integer.parseInt(day) == gui.getCurrentDay() && year == gui.getCurrentYear())
                        gui.dayEventsTable.setItems(hc.handleChangeDayRequest());
                    
                    //and mark it as edited
                    gui.getFileController().markAsEdited(gui);
                }
            }
        }
        else {
            eorcd.showRemoveChoiceDialog();
            
            //did the user confirm?
            if (eorcd.wasCompleteSelected()) {
                //make sure the user really means it
                yncd.show(PropertiesManager.getPropertiesManager().getProperty(CAL_PropertyType.REMOVE_EVENT_MESSAGE));
                
                String selection = yncd.getSelection();
                
                if (selection.equals(YesNoCancelDialog.YES)) {
                    //get the event
                    Event e = eorcd.getEvent();
                    
                    //get the month's list to remove the event
                    hc.getMonthList().remove(e);
                    hc.getHashValues().getAllNewEvents().remove(e);
                    Collections.sort(hc.getHashValues().getAllNewEvents());
                    
                    //update the events label
                    fillEventsLabel();
                    
                    //dehighlight the day if there is no more events
                    if (listOfEventsLabel.getText().equals(""))
                        eventButton.getStyleClass().remove(CLASS_EVENTS_REGULAR);
                    
                    //and update the table if it was from today
                    if (month == gui.getIntCurrentMonth() && Integer.parseInt(day) == gui.getCurrentDay() && year == gui.getCurrentYear())
                        gui.dayEventsTable.setItems(hc.handleChangeDayRequest());
                    
                    //and mark it as edited
                    gui.getFileController().markAsEdited(gui);
                }
            }
        }
    }
}