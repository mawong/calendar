package calendar.gui;

import calendar.CAL_PropertyType;
import calendar.controller.EventController;
import calendar.data.Event;
import static calendar.gui.CAL_GUI.CLASS_PROMPT_LABEL;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class prints a dialog box to the screen asking the user to enter which option
 * he or she would like to edit or remove.
 * 
 * @author MAVIRI
 */
public class EditOrRemoveChoiceDialog extends Stage {
    //HashController and components for the UI
    EventController hc;
    PropertiesManager props;
    Event event;
    
    GridPane gridPane;
    Scene dialogScene;
    Label messageLabel;
    TextField optionTextField;
    Button completeButton;
    Button cancelButton;
    
    //keep track of which button is pressed and which option the user wants
    String selection;
    String text;
    int choice;
    
    //constants
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String EDIT_OPTION = "Which event would you like to edit?";
    public static final String REMOVE_OPTION = "Which event would you like to remove?";
    
    /**
     * Constructor that creates the dialog box.
     * 
     * @param primaryStage - the stage the user is currently viewing
     * @param messageDialog - the message dialog
     * @param initHC - the hash controller
     * @param ded - the day events dialog
     */
    public EditOrRemoveChoiceDialog(Stage primaryStage, MessageDialog messageDialog, EventController initHC, DayEventsDialog ded) {
        hc = initHC;
        
        //make it modal (others will wait when this is displayed)
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //create the container
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        props = PropertiesManager.getPropertiesManager();
        
        //and the message label
        messageLabel = new Label();
        messageLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
        //and the text field
        optionTextField = new TextField();
        
        //and the buttons
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        //register the textfield
        optionTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            text = optionTextField.textProperty().getValue();
        });
        
        //and the buttons
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            EditOrRemoveChoiceDialog.this.selection = sourceButton.getText();
            if (selection.equals(COMPLETE)) {
                boolean good = checkValidity(messageDialog, ded);
                if (good)
                    EditOrRemoveChoiceDialog.this.hide();
            }
            else
                EditOrRemoveChoiceDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //now, arrange everything on the grid
        gridPane.add(messageLabel, 0, 0, 4, 1);
        gridPane.add(optionTextField, 0, 1, 4, 1);
        gridPane.add(completeButton, 0, 2, 1, 1);
        gridPane.add(cancelButton, 1, 2, 1, 1);
        
        //and put the dialog scene in the window
        dialogScene = new Scene(gridPane);
        this.setScene(dialogScene);
    }
    
    /**
     * This accessor method gets the selection the user pressed.
     * 
     * @return the selection the user pressed
     */
    public String getSelection() {
        return selection;
    }
    
    /**
     * This method shows the edit choice dialog box. The user will enter which choice
     * he or she would like to edit. There is foolproof design for this.
     */
    public void showEditChoiceDialog() {
        setTitle(props.getProperty(CAL_PropertyType.EDIT_EVENT_TITLE));
        messageLabel.setText(EDIT_OPTION);
        optionTextField.setText("");
        text = "";
        choice = 0;
        this.showAndWait();
    }
    
    /**
     * This method shows the remove choice dialog box.  The user will enter which choice
     * he or she would like to remove. There is foolproof design for this.
     */
    public void showRemoveChoiceDialog() {
        setTitle(props.getProperty(CAL_PropertyType.REMOVE_EVENT_TITLE));
        messageLabel.setText(REMOVE_OPTION);
        optionTextField.setText("");
        text = "";
        choice = 1;
        this.showAndWait();
    }
    
    /**
     * This method checks to see if complete was selected.
     * 
     * @return true if complete was selected, false otherwise
     */
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    /**
     * This accessor method gets the specified event.
     * 
     * @return the event we made/edited
     */
    public Event getEvent() {
        return event;
    }
    
    /**
     * This method checks to see if what the user entered was a valid option. Adds foolproof design.
     * 
     * @param messageDialog - the message dialog
     * @param ded - the day events dialog box
     * @return 
     */
    private boolean checkValidity(MessageDialog messageDialog, DayEventsDialog ded) {
        //nothing in the text field?
        if (text.equals("")) {
            messageDialog.show(props.getProperty(CAL_PropertyType.TEXT_FIELD_EMPTY_MESSAGE));
            return false;
        }
        else {
            //check if the text field has numbers
            for (int i = 0; i < text.length(); i++) {
                char ch = text.charAt(i);
                if (!Character.isDigit(ch)) {
                    messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_EDIT_REMOVE_TEXT_FIELD_MESSAGE));
                    return false;
                }
            }
            //get the option number
            int option = Integer.parseInt(text);
            option--;
            //invalid -- arrayIndexOutOfBounds
            if (option < 0) {
                messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_EDIT_REMOVE_TEXT_FIELD_MESSAGE));
                    return false;
            }
            
            String[] events = ded.listOfEventsLabel.getText().split("\n");
            //invalid -- arrayIndexOutOfBounds
            if (events.length <= option) {
                messageDialog.show(props.getProperty(CAL_PropertyType.INVALID_EDIT_REMOVE_TEXT_FIELD_MESSAGE));
                    return false;
            }
            
            String[] eventData = events[option].split("\t");    //0=listNum, 1=time, 2="", 3=activity
            ObservableList list = hc.getMonthList();
            for (Object list1 : list) {
                Event e = (Event) list1;
                if (e.getStartTime().equals(eventData[1]) && e.getActivity().equals(eventData[3])) {
                    //invalid -- preset event
                    if (e.getIsPreset()) {
                        if (choice == 0) {
                            messageDialog.show(props.getProperty(CAL_PropertyType.CANNOT_EDIT_EVENT_MESSAGE));
                            return false;
                        }
                        else {
                            messageDialog.show(props.getProperty(CAL_PropertyType.CANNOT_REMOVE_EVENT_MESSAGE));
                            return false;
                        }
                    }
                    //we got the correct event
                    event = e;
                    break;
                }
            }
        }
        return true;
    }
}