package calendar.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This dialog handles simple messages to the user when events occur.
 *
 * @author MAVIRI
 */
public class MessageDialog extends Stage {
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button okButton;
    
    /**
     * Constructor initializes a dialog so it can be used repeatedly for all kinds of messages.
     * 
     * @param owner - the window the user is currently looking at
     * @param text - the text for the OK button
     */
    public MessageDialog(Stage owner, String text) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        //message label to display
        messageLabel = new Label();
        
        //the button to close
        okButton = new Button(text);
        okButton.setOnAction(e -> { MessageDialog.this.close(); });
        
        //put everything in the pane
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(okButton);
        
        //padding -- look nice
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);
        
        //put in window
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
    
    /**
     * This method loads a custom message and then pops open the dialog.
     * 
     * @param message - the message to be loaded
     */
    public void show(String message) {
        messageLabel.setText(message);
        this.showAndWait();
    }
}