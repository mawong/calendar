package calendar.init;

import calendar.CAL_PropertyType;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import properties_manager.PropertiesManager;

/**
 * This class initializes all the labels and buttons necessary for the calendar GUI.
 * It is easier to separate the classes for organization; this should make it easier.
 * Note that this is just an initialization class; a helper class that offers utilities.
 *
 * @author MAVIRI
 */
public class InitializeGui {
    //default constructor
    public InitializeGui() {}
    
    /**
     * This method creates a button and puts it on the grid.
     * 
     * @param container - the gridpane we are placing the button in
     * @param tooltip - the tooltip we are using for the button
     * @param col - the column number, starting with index 0
     * @param row - the row number, starting with index 0
     * @param colSpan - the column span
     * @param rowSpan - the row span
     * @return the button created
     */
    public Button initGridButton(GridPane container, CAL_PropertyType tooltip, int col, int row, int colSpan, int rowSpan) {
       PropertiesManager props = PropertiesManager.getPropertiesManager();
        Button button = new Button();
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        button.setMinWidth(60);
        button.setMinHeight(60);
        button.setAlignment(Pos.CENTER);
        button.setDisable(true);
        container.add(button, col, row, colSpan, rowSpan);
        return button;
    }
    
    /**
     * This method creates a button that has a tooltip and places it into a grid.
     * 
     * @param container - the gridpane
     * @param text - the button's text
     * @param tooltip - the button's tooltip
     * @param col - the column number, starting at index 0
     * @param row - the row number, starting at index 0
     * @param colSpan - the column span
     * @param rowSpan - the row span
     * @return the button we created
     */
    public Button initChildTooltipButton(GridPane container, CAL_PropertyType text, CAL_PropertyType tooltip, int col, int row, int colSpan, int rowSpan) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Button button = new Button();
        String buttonText = props.getProperty(text.toString());
        button.setText(buttonText);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        container.add(button, col, row, colSpan, rowSpan);
        return button;
    }
    
    /**
     * This method initializes a label and its stylesheet.
     * 
     * @param labelProperty - the label's text
     * @param styleClass - the label's styleclass
     * @return the label we created
     */
    public Label initLabel(CAL_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    /**
     * This method places a label in the grid.
     * 
     * @param container - the gridpane.
     * @param labelProperty - the label's text
     * @param styleClass - the label's styleclass
     * @param col - the column number, starting at index 0
     * @param row - the row number, starting at index 0
     * @param colSpan - the column span
     * @param rowSpan - the row span
     * @return the label we created
     */
    public Label initGridLabel(GridPane container, CAL_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        label.setMinWidth(60);
        label.setAlignment(Pos.CENTER);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }
}